INSERT INTO parameter (id, code, name, description, flag_active, create_by, create_date,update_by, update_date, version) 
       VALUES (12, 'FLW001','System Apply Flow', 'System Apply Flow', '1', 'admin', NULL, 'admin', NULL, 0);

INSERT INTO parameter_detail (id, code, name, description, flag_active, variable1, variable2, variable3, variable4, variable5, variable6, variable7, variable8, variable9, variable10, parameter, create_by, create_date,update_by, update_date, version) 
	   VALUES (1201, 'ITCANE', 'IT Cane', 'IT Cane', '1', 'http://localhost:8090/mailEngine/sendBasicMailByTemplate/', 'ITCANE1', 'itcane@gmail.com', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, 12, 'admin', NULL, 'admin', NULL, 0);

INSERT INTO flow_config (id, code,sequence_display, name, description, flag_active,system_code, create_by, create_date,update_by, update_date, version) 
       VALUES (1, 'F001',1,'Flow 1', 'Flow 1', '1','ITCANE', 'admin', NULL, 'admin', NULL, 0);

       
INSERT INTO flow_config_detail (id,flag_active,sequence,role_code,role_name, flow_config, create_by, create_date,update_by, update_date, version) 
	   VALUES (1001, '1',1,'VRF','Verify', 1, 'admin', NULL, 'admin', NULL, 0);
INSERT INTO flow_config_detail (id,  flag_active,sequence,role_code,role_name, flow_config, create_by, create_date,update_by, update_date, version) 
	   VALUES (1002, '1',2,'APR','Approve', 1, 'admin', NULL, 'admin', NULL, 0);

       
       
