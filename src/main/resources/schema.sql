CREATE TABLE IF NOT EXISTS parameter (
	id bigint NOT NULL ,
	code varchar(255) NULL ,
	name varchar(255) NULL ,
	description varchar(255) NULL ,
	flag_active bit NULL ,
	create_by varchar(255) NULL ,
	create_date datetime2(7) NULL ,
	update_by varchar(255) NULL ,
	update_date datetime2(7) NULL ,
	version bigint NULL 
);

CREATE TABLE IF NOT EXISTS  parameter_detail (
	id bigint NOT NULL ,
	code varchar(255) NULL ,
	name varchar(255) NULL ,
	description varchar(255) NULL ,
	flag_active bit NULL ,
	variable1 varchar(255) NULL ,
	variable2 varchar(4000) NULL ,
	variable3 varchar(255) NULL ,
	variable4 varchar(255) NULL ,
	variable5 varchar(255) NULL ,
	variable6 varchar(255) NULL ,
	variable7 varchar(255) NULL ,
	variable8 varchar(255) NULL ,
	variable9 varchar(255) NULL ,
	variable10 varchar(255) NULL  ,
	parameter bigint NULL ,
	create_by varchar(255) NULL ,
	create_date datetime2(7) NULL ,
	update_by varchar(255) NULL ,
	update_date datetime2(7) NULL ,
	version bigint NULL
);

CREATE TABLE IF NOT EXISTS  flow_config (
	id bigint NOT NULL ,
	code varchar(255) NULL ,
	name varchar(255) NULL ,
	description varchar(255) NULL ,
	sequence_display int NOT NULL ,
	flag_active bit NULL ,
	system_code  varchar(255) NULL ,
	create_by varchar(255) NULL ,
	create_date datetime2(7) NULL ,
	update_by varchar(255) NULL ,
	update_date datetime2(7) NULL ,
	version bigint NULL 
);

CREATE TABLE IF NOT EXISTS  flow_config_detail (
	id bigint NOT NULL ,
	flag_active bit NULL ,
	sequence int NOT NULL ,
	role_code varchar(255) NULL ,
	role_name varchar(255) NULL ,
	flow_config bigint NULL ,
	create_by varchar(255) NULL ,
	create_date datetime2(7) NULL ,
	update_by varchar(255) NULL ,
	update_date datetime2(7) NULL ,
	version bigint NULL
);

CREATE TABLE IF NOT EXISTS  process_flow (
	id bigint NOT NULL ,
	flow_code varchar(255) NULL ,
	document_number varchar(255) NULL ,
	description varchar(255) NULL ,
	flag_active bit NULL ,
	creater varchar(255) NULL ,
	creater_email varchar(255) NULL ,
	requester varchar(255) NULL ,
	requester_email varchar(255) NULL ,
	create_by varchar(255) NULL ,
	create_date datetime2(7) NULL ,
	update_by varchar(255) NULL ,
	update_date datetime2(7) NULL ,
	version bigint NULL 
);

CREATE TABLE IF NOT EXISTS  flow_task (
	id bigint NOT NULL ,
	sequence int NOT NULL ,
	action_state_code varchar(255) NULL ,
	action_state_name varchar(255) NULL ,
	approver_user_name varchar(255) NULL ,
	approver_user_id varchar(255) NULL ,
	approver_user_email varchar(255) NULL ,
    task_status varchar(255) NULL ,
	assign_time datetime2(7) NULL ,
	action_time datetime2(7) NULL ,
	process_flow bigint NULL ,
	create_by varchar(255) NULL ,
	create_date datetime2(7) NULL ,
	update_by varchar(255) NULL ,
	update_date datetime2(7) NULL ,
	version bigint NULL
);