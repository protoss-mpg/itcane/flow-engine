package com.mpg.itcane.flow;

public class ApplicationException extends RuntimeException{

	public ApplicationException(String message) {
		super(message);
	}

	
}
