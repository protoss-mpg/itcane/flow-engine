package com.mpg.itcane.flow.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mpg.itcane.flow.ApplicationConstant;
import com.mpg.itcane.flow.ApplicationException;
import com.mpg.itcane.flow.entity.FlowConfig;
import com.mpg.itcane.flow.entity.FlowConfigDetail;
import com.mpg.itcane.flow.entity.FlowTask;
import com.mpg.itcane.flow.entity.ParameterDetail;
import com.mpg.itcane.flow.entity.ProcessFlow;
import com.mpg.itcane.flow.model.RequestFlowDetailModel;
import com.mpg.itcane.flow.model.RequestFlowModel;
import com.mpg.itcane.flow.model.ResponseModel;
import com.mpg.itcane.flow.repository.FlowConfigRepository;
import com.mpg.itcane.flow.repository.FlowTaskRepository;
import com.mpg.itcane.flow.repository.ParameterDetailRepository;
import com.mpg.itcane.flow.repository.ParameterRepository;
import com.mpg.itcane.flow.repository.ProcessFlowRepository;
import com.mpg.itcane.flow.service.ProcessFlowService;
import com.mpg.itcane.flow.service.SendMailService;
import com.mpg.itcane.flow.util.CommonUtil;
import com.mpg.itcane.flow.util.JSONUtil;
import com.mpg.itcane.flow.util.RequestUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProcessFlowServiceImpl implements ProcessFlowService {

	 @Autowired
	 ParameterRepository parameterRepository;

	 @Autowired
	 ParameterDetailRepository parameterDetailRepository;
	 
	 @Autowired
	 ProcessFlowRepository processFlowRepository;
	 
	 @Autowired
	 FlowTaskRepository taskRepository;
	 
	 @Autowired
	 FlowConfigRepository flowConfigRepository;
	 
	 @Autowired
	 SendMailService sendMailService;
	 
	 private void validateConfig(Set<FlowConfigDetail> flowConfigDetailSet,RequestFlowModel requestFlowModel) {
		 if(flowConfigDetailSet==null || flowConfigDetailSet.isEmpty()){
				throw new ApplicationException("FlowConfigDetail is empty");
			}else if(requestFlowModel == null || requestFlowModel.getDetails().isEmpty()){
				throw new ApplicationException("RequestFlowDetail@JSON Request is empty");
			}else if(flowConfigDetailSet.size() != requestFlowModel.getDetails().size()){
				throw new ApplicationException("Size of config and request not matched");
			}
	 }
	 
	public ResponseModel createProcess(String flowCode,String systemCode,String username,RequestFlowModel requestFlowModel) {
		ResponseModel response = new ResponseModel();
		try {
			String templateCode    = "";
			
			/* Find Paramter Setup of System Use Flow */
			ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_FLOW_SYSTEM_USE_FLOW),systemCode);
			String urlMailServer		= parameterDetail.getVariable1();
			String providerCode 		= parameterDetail.getVariable2();
			String mailFromAddress 		= parameterDetail.getVariable3();
			String mailFlagActive 		= parameterDetail.getVariable4();
        	
			/* Define Parameter Email */
			Map<String, String> parameterSendMailMap = new HashMap();
			
			FlowConfig flowConfig = flowConfigRepository.findByCodeAndSystemCode(flowCode, systemCode);
			
			/* Set Data from request to  ProcessFlow */
			Set<FlowConfigDetail> flowConfigDetailSet = flowConfig.getDetails();
			String createBy = RequestUtil.getValueString(username, ApplicationConstant.FIELD_CREATE_BY_DEFAULT);
			
			Timestamp createDate = CommonUtil.getCurrentDateTimestamp();
			ProcessFlow processFlow = new ProcessFlow();
			processFlow.setFlowCode(flowCode);
			processFlow.setDocumentNumber(requestFlowModel.getDocumentNumber());
			processFlow.setDescription(requestFlowModel.getDescription());
			processFlow.setProcessStatus(ApplicationConstant.PROCESS_STATUS_CREATED);
			processFlow.setCreater(requestFlowModel.getCreater());
			processFlow.setCreaterEmail(requestFlowModel.getCreaterEmail());
			processFlow.setRequester(requestFlowModel.getRequester());
			processFlow.setRequesterEmail(requestFlowModel.getRequesterEmail());
			processFlow.setCreateBy(createBy);
			processFlow.setCreateDate(createDate);
			processFlowRepository.saveAndFlush(processFlow);
			
			/* Validate Data */
			this.validateConfig(flowConfigDetailSet, requestFlowModel);
			
			/* Fetch From Config to Flow Task */
			Set<FlowTask> taskSet = new HashSet();
			for(FlowConfigDetail flowConfigDetail:flowConfigDetailSet){
				/* Check Config Match Param Body by sequence */
				for(RequestFlowDetailModel requestFlowDetail:requestFlowModel.getDetails()){
					
					if(flowConfigDetail.getSequence().equals(requestFlowDetail.getSequence())){
						
						if(!flowConfigDetail.getRoleCode().equals(requestFlowDetail.getRoleFlow())){
							throw new ApplicationException("Role(Body parameter) Not Match in Config");
						}
						
						/* Set Data from request to  FlowTask */
						FlowTask task = new FlowTask();
						task.setSequence(flowConfigDetail.getSequence());
						task.setActionStateCode(flowConfigDetail.getRoleCode());
						task.setActionStateName(flowConfigDetail.getRoleName());
						task.setApproverUserName(requestFlowDetail.getUsernameApprover());
						task.setApproverUserId(requestFlowDetail.getUserId());
						task.setApproverUserEmail(requestFlowDetail.getEmail());
						task.setTaskStatus(ApplicationConstant.TASK_STATUS_NOT_ARRIVE);
						task.setProcessFlow(processFlow);
						task.setCreateBy(createBy);
						task.setCreateDate(createDate);
						
						/* Start Flow */
						if(flowConfigDetail.getSequence()==1){
							task.setTaskStatus(ApplicationConstant.TASK_STATUS_WAIT);
							task.setAssignTime(createDate);
							/* Pass Parameter to email */
							parameterSendMailMap.put(ApplicationConstant.FIX_ADDRESS_TO, requestFlowDetail.getEmail());
							parameterSendMailMap.put(ApplicationConstant.FIX_CC_ADDRESS, requestFlowModel.getRequesterEmail());
							parameterSendMailMap.put(ApplicationConstant.FIX_APPROVER_NAME, requestFlowDetail.getUsernameApprover());
							parameterSendMailMap.put(ApplicationConstant.FIX_ACTION_STATE_CODE, flowConfigDetail.getRoleCode());
							parameterSendMailMap.put(ApplicationConstant.FIX_ACTION_STATE_NAME, flowConfigDetail.getRoleName());
							parameterSendMailMap.put(ApplicationConstant.FIX_APPROVER_USER_ID, requestFlowDetail.getUserId());
							parameterSendMailMap.put(ApplicationConstant.FIX_TASK_STATUS, ApplicationConstant.TASK_STATUS_WAIT);
						}
						
						
						taskRepository.saveAndFlush(task);
						
						taskSet.add(task);
						
						break;
					}
					
				}
			}
			
			processFlow.setTasks(taskSet);
			
			/* Send Email To Next Approver ,cc Requester and Creater */
			templateCode = providerCode+"_"+ApplicationConstant.PROCESS_STATUS_CREATED;
			if(urlMailServer!=null && !urlMailServer.isEmpty() && "Y".equals(mailFlagActive)) {
				parameterSendMailMap = this.sendMail(parameterSendMailMap, mailFromAddress, providerCode, templateCode, urlMailServer);
			}
			
			response.setSuccess(true);
			response.setMessage("Process id="+processFlow.getId());
			
			/* Setup Return Data */
			Map<String,Object> data = new HashMap();
			data.put(ApplicationConstant.FIX_PROCESS_DATA,   processFlow);
			data.put(ApplicationConstant.FIX_EMAIL_DATA, parameterSendMailMap);
			response.setData(data);
		} catch (Exception e) {
			response.setMessage(e.getMessage());
		}
		
		return response;
	}

    
	
	@Override
	public ResponseModel approveProcess(String systemCode,Long processId, Long taskId, String username) {
		Timestamp currentTime = CommonUtil.getCurrentDateTimestamp();
		ResponseModel response = new ResponseModel();
		
		ProcessFlow processFlow = null;
		try {
			String templateCode    = "";
			/* Define Parameter Email */
			Map<String, String> parameterSendMailMap = new HashMap();
			
			/* Find Paramter Setup of System Use Flow */
			ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_FLOW_SYSTEM_USE_FLOW),systemCode);
			String urlMailServer		= parameterDetail.getVariable1();
			String providerCode 		= parameterDetail.getVariable2();
			String mailFromAddress 	    = parameterDetail.getVariable3();
			String mailFlagActive 		= parameterDetail.getVariable4();
	        
			
			
			/* Check ProcessFole in DB */
			processFlow = processFlowRepository.getOne(processId);
			
			/* Validate Data */
			validateProcessStatus(processFlow,processId,"approved");
			
			/* Set Current Sequence */
			Integer nextSequence = 0;
			Integer maxSequence = 0;
			Integer currentSequence = 0;
			Boolean hasTaskMatchRequest = false;
			/* Get Max Sequence*/
			for(FlowTask task :processFlow.getTasks()){
				if(task.getSequence() > maxSequence){
					maxSequence = task.getSequence();
				}
			}
			/* Validate and Update Data */
			for(FlowTask task :processFlow.getTasks()){
				if(task.getId().equals(taskId)){
					
					/* Validate Task */
					validateTask(task,username);
					
					task.setTaskStatus(ApplicationConstant.TASK_STATUS_APPROVE);
					task.setActionTime(currentTime);
					task.setUpdateBy(username);
					task.setUpdateDate(currentTime);
					
					
					currentSequence = task.getSequence();
					nextSequence = task.getSequence() + 1;
					
					hasTaskMatchRequest = true;
					break;
				}
			}
			
			if(!hasTaskMatchRequest){
				throw new ApplicationException("Task ="+taskId+" not found");
			}
			
			/* Set Next Sequence */
			for(FlowTask task :processFlow.getTasks()){
				if(task.getSequence().equals(nextSequence)){
					task.setTaskStatus(ApplicationConstant.TASK_STATUS_WAIT);
					task.setAssignTime(currentTime);
					
					/* Pass Parameter to email */
					parameterSendMailMap.put(ApplicationConstant.FIX_ADDRESS_TO, task.getApproverUserEmail());
					parameterSendMailMap.put(ApplicationConstant.FIX_CC_ADDRESS, task.getProcessFlow().getRequesterEmail());
					parameterSendMailMap.put(ApplicationConstant.FIX_APPROVER_NAME, task.getApproverUserName());
					parameterSendMailMap.put(ApplicationConstant.FIX_ACTION_STATE_CODE, task.getActionStateCode());
					parameterSendMailMap.put(ApplicationConstant.FIX_ACTION_STATE_NAME, task.getActionStateName());
					parameterSendMailMap.put(ApplicationConstant.FIX_APPROVER_USER_ID, task.getApproverUserId());
					parameterSendMailMap.put(ApplicationConstant.FIX_TASK_STATUS, ApplicationConstant.TASK_STATUS_WAIT);
					
					break;
				}
			}
			/* Persist Task */
			taskRepository.saveAll(processFlow.getTasks());
			
			/* When Task is final then update ProcessFlow status to complete */
			if(currentSequence.equals(maxSequence)){
				processFlow.setUpdateBy(username);
				processFlow.setUpdateDate(currentTime);
				processFlow.setProcessStatus(ApplicationConstant.PROCESS_STATUS_COMPLETE);
				processFlowRepository.save(processFlow);
				
				/* Send Email complete information To Requester   */
				parameterSendMailMap.put(ApplicationConstant.FIX_ADDRESS_TO, parameterSendMailMap.get(ApplicationConstant.FIX_CC_ADDRESS));
				parameterSendMailMap.put(ApplicationConstant.FIX_TASK_STATUS, ApplicationConstant.TASK_STATUS_APPROVE);
				

				templateCode = providerCode+"_"+ApplicationConstant.PROCESS_STATUS_COMPLETE;
				
			}else{
				/* Send Email To Next Approver ,cc Requester and Creater */
				templateCode = providerCode+"_"+ApplicationConstant.PROCESS_STATUS_CREATED;
				
			}

			if(urlMailServer!=null && !urlMailServer.isEmpty() && "Y".equals(mailFlagActive)) {
				parameterSendMailMap = this.sendMail(parameterSendMailMap, mailFromAddress, providerCode, templateCode, urlMailServer);
			}
			
			
			response.setSuccess(true);
			response.setMessage("Approve successful.");
			/* Setup Return Data */     
			response = this.buildDataObject(response, processFlow, parameterSendMailMap);
		} catch (Exception e) {
			response.setMessage(e.getMessage());
		}
		return response;
	}
	
	private ResponseModel buildDataObject(ResponseModel response,ProcessFlow processFlow,Map<String, String> parameterSendMailMap) {
		Map<String,Object> data = new HashMap();
		data.put(ApplicationConstant.FIX_PROCESS_DATA,   processFlow);
		data.put(ApplicationConstant.FIX_EMAIL_DATA, parameterSendMailMap);
		response.setData(JSONUtil.mapFromJson(JSONUtil.getJSONSerializer().include(ApplicationConstant.FIX_PROCESS_DATA_TASK).serialize(data)) );
	
		return response;
	}


	@Override
	public ResponseModel rejectProcess(String systemCode,Long processId, Long taskId, String username) {
		Timestamp currentTime = CommonUtil.getCurrentDateTimestamp();
		ResponseModel response = new ResponseModel();
		ProcessFlow processFlow = null;
		try {
			
			String templateCode    = "";
			
			processFlow = processFlowRepository.getOne(processId);
			
			/* Find Paramter Setup of System Use Flow */
			ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_FLOW_SYSTEM_USE_FLOW),systemCode);
			String urlMailServer		= parameterDetail.getVariable1();
			String providerCode 		= parameterDetail.getVariable2();
			String mailFromAddress 	    = parameterDetail.getVariable3();
			String mailFlagActive 		= parameterDetail.getVariable4();
			
			/* Check ProcessFole in DB */
			Map<String, String> parameterSendMailMap = new HashMap();
			
			
			/* Validate Data */
			validateProcessStatus(processFlow,processId,"rejected");
			
			
			/* Set Current Sequence */
			Integer maxSequence = 0;
			Boolean hasTaskMatchRequest = false;
			for(FlowTask task :processFlow.getTasks()){
				if(task.getId().equals(taskId)){
					/* Validate Task */
					validateTask(task,username);
					
					task.setTaskStatus(ApplicationConstant.TASK_STATUS_REJECT);
					task.setActionTime(currentTime);
					task.setUpdateBy(username);
					task.setUpdateDate(currentTime);
					
					/* Pass Parameter to email */
					parameterSendMailMap.put(ApplicationConstant.FIX_ADDRESS_TO, task.getProcessFlow().getRequesterEmail());
					parameterSendMailMap.put(ApplicationConstant.FIX_CC_ADDRESS, task.getProcessFlow().getCreaterEmail());
					parameterSendMailMap.put(ApplicationConstant.FIX_APPROVER_NAME, task.getApproverUserName());
					parameterSendMailMap.put(ApplicationConstant.FIX_ACTION_STATE_CODE, task.getActionStateCode());
					parameterSendMailMap.put(ApplicationConstant.FIX_ACTION_STATE_NAME, task.getActionStateName());
					parameterSendMailMap.put(ApplicationConstant.FIX_APPROVER_USER_ID, task.getApproverUserId());
					parameterSendMailMap.put(ApplicationConstant.FIX_TASK_STATUS, ApplicationConstant.TASK_STATUS_REJECT);
					
					
					hasTaskMatchRequest = true;
					break;
				}
				
				if(task.getSequence() > maxSequence){
					maxSequence = task.getSequence();
				}
			}
			
			if(!hasTaskMatchRequest){
				throw new ApplicationException("Task ="+taskId+" not found");
			}
			
			
			/* Persist Task */
			taskRepository.saveAll(processFlow.getTasks());
			
			/* Update Status of Process*/
			processFlow.setUpdateBy(username);
			processFlow.setUpdateDate(currentTime);
			processFlow.setProcessStatus(ApplicationConstant.PROCESS_STATUS_REJECT);
			processFlowRepository.save(processFlow);
			
			/* Send Email To Requester and cc Creater */
			templateCode = providerCode+"_"+ApplicationConstant.PROCESS_STATUS_REJECT;
			if(urlMailServer!=null && !urlMailServer.isEmpty() && "Y".equals(mailFlagActive)) {
				parameterSendMailMap = this.sendMail(parameterSendMailMap, mailFromAddress, providerCode, templateCode, urlMailServer);
			}
			
			response.setSuccess(true);
			response.setMessage("Reject successful.");
			
			/* Setup Return Data */
			this.buildDataObject(response, processFlow, parameterSendMailMap);
			
		} catch (Exception e) {
			response.setMessage(e.getMessage());
		}
		return response;
	}


	@Override
	public ResponseModel cancelProcess(String systemCode,Long processId, Long taskId, String username) {
		Timestamp currentTime = CommonUtil.getCurrentDateTimestamp();
		ResponseModel response = new ResponseModel();
		ProcessFlow processFlow = null;
		try {
			
			String templateCode    = "";
			
			/* Find Paramter Setup of System Use Flow */
			ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_FLOW_SYSTEM_USE_FLOW),systemCode);
			String urlMailServer		= parameterDetail.getVariable1();
			String providerCode 		= parameterDetail.getVariable2();
			String mailFromAddress 	    = parameterDetail.getVariable3();
			String mailFlagActive 		= parameterDetail.getVariable4();
	        
			Map<String, String> parameterSendMailMap = new HashMap();
			
			/* Check ProcessFole in DB */
			processFlow = processFlowRepository.getOne(processId);
			
			/* Validate Data */
			if(!processFlow.getCreater().equals(username) && !processFlow.getRequester().equals(username)){
				throw new ApplicationException("User Access denie to cancel");
			}
			validateProcessStatus(processFlow,processId,"canceled");
			
			
			parameterSendMailMap.put(ApplicationConstant.FIX_CC_ADDRESS,processFlow.getCreaterEmail()+","+processFlow.getRequesterEmail());
			for(FlowTask task :processFlow.getTasks()){
				if(ApplicationConstant.TASK_STATUS_APPROVE.equals(task.getTaskStatus())){
					parameterSendMailMap.put(ApplicationConstant.FIX_CC_ADDRESS, parameterSendMailMap.get(ApplicationConstant.FIX_CC_ADDRESS)+","+task.getApproverUserEmail());
				}
				if(ApplicationConstant.TASK_STATUS_WAIT.equals(task.getTaskStatus())){
					parameterSendMailMap.put(ApplicationConstant.FIX_ADDRESS_TO, task.getApproverUserEmail());
					parameterSendMailMap.put(ApplicationConstant.FIX_APPROVER_NAME, task.getApproverUserName());
					parameterSendMailMap.put(ApplicationConstant.FIX_ACTION_STATE_CODE, task.getActionStateCode());
					parameterSendMailMap.put(ApplicationConstant.FIX_ACTION_STATE_NAME, task.getActionStateName());
					parameterSendMailMap.put(ApplicationConstant.FIX_APPROVER_USER_ID, task.getApproverUserId());
					parameterSendMailMap.put(ApplicationConstant.FIX_TASK_STATUS, ApplicationConstant.TASK_STATUS_WAIT);
				}
			}
			
			processFlow.setUpdateBy(username);
			processFlow.setUpdateDate(currentTime);
			processFlow.setProcessStatus(ApplicationConstant.PROCESS_STATUS_CANCEL);
			processFlowRepository.save(processFlow);
			
			/* Send Email To Requester and cc Creater,Approver passed,Approver Current */
			templateCode = providerCode+"_"+ApplicationConstant.PROCESS_STATUS_CANCEL;
			if(urlMailServer!=null && !urlMailServer.isEmpty() && "Y".equals(mailFlagActive)) {
				parameterSendMailMap = this.sendMail(parameterSendMailMap, mailFromAddress, providerCode, templateCode, urlMailServer);
			}
			
			response.setSuccess(true);
			response.setMessage("Cancel successful.");
			/* Setup Return Data */
			response = this.buildDataObject(response, processFlow, parameterSendMailMap);
		} catch (Exception e) { response.setMessage(e.getMessage()); }
		return response;
	}

	private Map<String, String> sendMail(Map<String, String> parameterSendMailMap, 
			String mailFromAddress, 
			String providerCode, 
			String templateCode, 
			String urlMailServer) {
		parameterSendMailMap.put(ApplicationConstant.FIX_FROM,         mailFromAddress);
		parameterSendMailMap.put(ApplicationConstant.FIX_FROM_ADDRESS,  mailFromAddress);
		parameterSendMailMap.put(ApplicationConstant.FIX_PROVIDER_CODE, providerCode);
		parameterSendMailMap.put(ApplicationConstant.FIX_TEMPLATE_CODE, templateCode);
		sendMailService.sendMail(parameterSendMailMap, urlMailServer+templateCode);
		return parameterSendMailMap;
	}

	@Override
	public ResponseModel terminateProcess(String systemCode,Long processId) {
		
		Timestamp currentTime = CommonUtil.getCurrentDateTimestamp();
		ResponseModel response = new ResponseModel();
		ProcessFlow processFlow = null;
		try {
			String templateCode    = "";
			Map<String, String> parameterSendMailMap = new HashMap();
			
			/* Find Paramter Setup of System Use Flow */
			ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_FLOW_SYSTEM_USE_FLOW),systemCode);
			String urlMailServer		= parameterDetail.getVariable1();
			String providerCode 		= parameterDetail.getVariable2();
			String mailFromAddress 		= parameterDetail.getVariable3();
			String mailFlagActive 		= parameterDetail.getVariable4();
			
			
			/* Check ProcessFole in DB */
			processFlow = processFlowRepository.getOne(processId);
			
			/* Validate Data */
			validateProcessStatus(processFlow,processId,"terminated");
			
			parameterSendMailMap.put(ApplicationConstant.FIX_ADDRESS_TO, processFlow.getRequesterEmail());
			parameterSendMailMap.put(ApplicationConstant.FIX_CC_ADDRESS, processFlow.getCreaterEmail());
			for(FlowTask task :processFlow.getTasks()){
				if(ApplicationConstant.TASK_STATUS_APPROVE.equals(task.getTaskStatus())){
					parameterSendMailMap.put(ApplicationConstant.FIX_CC_ADDRESS, parameterSendMailMap.get(ApplicationConstant.FIX_CC_ADDRESS)+","+task.getApproverUserEmail());
				}
				if(ApplicationConstant.TASK_STATUS_WAIT.equals(task.getTaskStatus())){
					parameterSendMailMap.put(ApplicationConstant.FIX_CC_ADDRESS, parameterSendMailMap.get(ApplicationConstant.FIX_CC_ADDRESS)+","+task.getApproverUserEmail());
					parameterSendMailMap.put(ApplicationConstant.FIX_APPROVER_NAME, task.getApproverUserName());
					parameterSendMailMap.put(ApplicationConstant.FIX_ACTION_STATE_CODE, task.getActionStateCode());
					parameterSendMailMap.put(ApplicationConstant.FIX_ACTION_STATE_NAME, task.getActionStateName());
					parameterSendMailMap.put(ApplicationConstant.FIX_APPROVER_USER_ID, task.getApproverUserId());
					parameterSendMailMap.put(ApplicationConstant.FIX_TASK_STATUS, ApplicationConstant.TASK_STATUS_WAIT);
				}
			}
			
			processFlow.setUpdateBy("admin");
			processFlow.setUpdateDate(currentTime);
			processFlow.setProcessStatus(ApplicationConstant.PROCESS_STATUS_TERMINATE);
			processFlowRepository.save(processFlow);
			
			/* Send Email To Requester and cc Creater,Approver passed,Approver Current */
			templateCode = providerCode+"_"+ApplicationConstant.PROCESS_STATUS_TERMINATE;
			if(urlMailServer!=null && !urlMailServer.isEmpty() && "Y".equals(mailFlagActive)) {
				parameterSendMailMap = this.sendMail(parameterSendMailMap, mailFromAddress, providerCode, templateCode, urlMailServer);
			}
			
			
			response.setSuccess(true);
			response.setMessage("Terminate successful.");
			
			/* Setup Return Data */
			response = this.buildDataObject(response, processFlow, parameterSendMailMap);
		} catch (Exception e) { response.setMessage(e.getMessage()); }
		
		return response;
	}


	@Override
	public ResponseModel findWaitingProcessByUserName(String username) {
		ResponseModel response = new ResponseModel();
		
		try {
			List<Map<String,Object>> taskList = new ArrayList();
			
			List<FlowTask> flowTaskLs = taskRepository.findByTaskStatusIgnoreCaseContainingAndApproverUserNameAndProcessFlowStatus(ApplicationConstant.TASK_STATUS_WAIT, username, ApplicationConstant.PROCESS_STATUS_CREATED);
			if(flowTaskLs != null && !flowTaskLs.isEmpty()){
				Map<String,Object> dataModel = null;
				for(FlowTask task:flowTaskLs){
					dataModel = new HashMap();
					dataModel.put(ApplicationConstant.FIX_PROCESS_ID, task.getProcessFlow().getId());
					dataModel.put(ApplicationConstant.FIX_TASK_ID, task.getId());
					dataModel.put(ApplicationConstant.FIX_DOCUMENT_NUMBER, task.getProcessFlow().getDocumentNumber());
					dataModel.put(ApplicationConstant.FIX_DESCRIPTION, task.getProcessFlow().getDescription());
					dataModel.put(ApplicationConstant.FIX_ACTION_STATE_CODE, task.getActionStateCode());
					dataModel.put(ApplicationConstant.FIX_ACTION_STATE_NAME, task.getActionStateName());
					taskList.add(dataModel);
				}
				
				response.setSuccess(true);
				response.setMessage("Has "+flowTaskLs.size()+" Task Wait for user="+username);
				response.setData(taskList);
				
			}else{
				response.setSuccess(true);
				response.setMessage(ApplicationConstant.FIX_NO_TASK_FOUND);
			}
		} catch (Exception e) { response.setMessage(e.getMessage()); }
		
		return response;
	}


	@Override
	public ResponseModel getCurrentTask(Long processId) {
		ResponseModel response = new ResponseModel();
		ProcessFlow processFlow = null;
		try {
			
			/* Check ProcessFole in DB */
			processFlow = processFlowRepository.getOne(processId);
			
			if(processFlow.getTasks().isEmpty()){
				throw new ApplicationException(ApplicationConstant.FIX_TASK_PROCESS_FLOW+processId+ApplicationConstant.FIX_IS_EMPTY);
			}
			
			/* Set Current Sequence */
			Map dataMap = null;
			for(FlowTask task :processFlow.getTasks()){
				if(ApplicationConstant.TASK_STATUS_WAIT.equals(task.getTaskStatus())){
					dataMap = JSONUtil.mapFromJson(JSONUtil.getJSONSerializer().serialize(task));
					response.setSuccess(true);
					response.setMessage("Task id="+task.getId());
					response.setData(dataMap);
					break;
				}
			}
		} catch (Exception e) { response.setMessage(e.getMessage()); }
		return response;
	}


	@Override
	public ResponseModel findWaitingProcessByUserNameAndFlowCode(String username, String flowCode) {
		ResponseModel response = new ResponseModel();
				
				try {
					List<Map<String,Object>> taskList = new ArrayList();
					
					List<FlowTask> flowTaskLs = taskRepository.findByTaskStatusIgnoreCaseContainingAndApproverUserNameAndProcessFlowStatusAndFlowCode(ApplicationConstant.TASK_STATUS_WAIT, username, ApplicationConstant.PROCESS_STATUS_CREATED,flowCode);
					if(flowTaskLs != null && !flowTaskLs.isEmpty()){
						Map<String,Object> dataModel = null;
						for(FlowTask task:flowTaskLs){
							dataModel = new HashMap();
							dataModel.put(ApplicationConstant.FIX_DESCRIPTION, task.getProcessFlow().getDescription());
							dataModel.put(ApplicationConstant.FIX_DOCUMENT_NUMBER, task.getProcessFlow().getDocumentNumber());
							dataModel.put(ApplicationConstant.FIX_ACTION_STATE_CODE, task.getActionStateCode());
							dataModel.put(ApplicationConstant.FIX_ACTION_STATE_NAME, task.getActionStateName());
							dataModel.put(ApplicationConstant.FIX_PROCESS_ID, task.getProcessFlow().getId());
							dataModel.put(ApplicationConstant.FIX_TASK_ID, task.getId());
							taskList.add(dataModel);
						}
						
						response.setSuccess(true);
						response.setMessage("Has "+flowTaskLs.size()+" Task Wait for user="+username);
						response.setData(taskList);
						
					}else{
						response.setMessage(ApplicationConstant.FIX_NO_TASK_FOUND);
						response.setSuccess(true);
					}
				} catch (Exception e) { response.setMessage(e.getMessage()); }
				
				
				
				return response;
	}
	
	@Override
	public ResponseModel findEndProcessAndTaskDetailByCreater(String creater) {
		return getResponseModelByProcessFlowList(processFlowRepository.findByCreaterAndProcessStatusNotIn(creater, Arrays.asList(ApplicationConstant.PROCESS_STATUS_CREATED)),
				creater,
				"tasks");
	}


	@Override
	public ResponseModel findEndProcessByCreater(String creater) {
		return getResponseModelByProcessFlowList(processFlowRepository.findByCreaterAndProcessStatusNotIn(creater, Arrays.asList(ApplicationConstant.PROCESS_STATUS_CREATED)),
				creater,
				"");
	}


	@Override
	public ResponseModel findEndProcessByCreaterAndFlowCode(String creater, String flowCode) {
		return getResponseModelByProcessFlowList(processFlowRepository.findByCreaterAndFlowCodeAndProcessStatusNotIn(creater,flowCode, Arrays.asList(ApplicationConstant.PROCESS_STATUS_CREATED)),
				creater,
				"");
	}


	@Override
	public ResponseModel findEndProcessAndTaskDetailByCreaterAndFlowCode(String creater, String flowCode) {
		return getResponseModelByProcessFlowList(processFlowRepository.findByCreaterAndFlowCodeAndProcessStatusNotIn(creater,flowCode, Arrays.asList(ApplicationConstant.PROCESS_STATUS_CREATED)),
				creater,
				"tasks");
	}

	public ResponseModel getResponseModelByProcessFlowList(List<ProcessFlow> processFlowLs,String username,String includeStr) {
		ResponseModel response = new ResponseModel();
		try {
			response.setSuccess(true);
			response.setMessage("Has "+processFlowLs.size()+" process of "+username);
			response.setData(JSONUtil.listFromJson(JSONUtil.getJSONSerializer().include(includeStr).serialize(processFlowLs)));
			
		} catch (Exception e) { response.setMessage(e.getMessage()); }
		
		return response;
	}
	
	private void validateProcessStatus(ProcessFlow processFlow,Long processId,String status) {
    	if(processFlow.getTasks().isEmpty()){
			throw new ApplicationException(ApplicationConstant.FIX_TASK_PROCESS_FLOW+processId+ApplicationConstant.FIX_IS_EMPTY);
		}else if(ApplicationConstant.PROCESS_STATUS_COMPLETE.equals(processFlow.getProcessStatus())){
			throw new ApplicationException("The process has been completed and cannot be "+status+".");
		}else if(ApplicationConstant.PROCESS_STATUS_CANCEL.equals(processFlow.getProcessStatus())){
			throw new ApplicationException("The process has been canceled and cannot be "+status+".");
		}else if(ApplicationConstant.PROCESS_STATUS_REJECT.equals(processFlow.getProcessStatus())){
			throw new ApplicationException("The process has been rejected and cannot be "+status+".");
		}else if(ApplicationConstant.PROCESS_STATUS_TERMINATE.equals(processFlow.getProcessStatus())){
			throw new ApplicationException("The process has been terminated and cannot be "+status+".");
		}
    }
    
    private void validateTask(FlowTask task, String username) {
    	if(!task.getApproverUserName().equals(username)){
			throw new ApplicationException("Approver User Name not matched");
		}else if(ApplicationConstant.TASK_STATUS_NOT_ARRIVE.equals(task.getTaskStatus())){
			throw new ApplicationException("for user "+username+",Task is not arrive");
		}else if(!ApplicationConstant.TASK_STATUS_WAIT.equals(task.getTaskStatus())){
			throw new ApplicationException("Task has passed you");
		}else if(task.getAssignTime() == null){
			throw new ApplicationException("Task not assign to you");
		}
    }
    


	private ResponseModel getResponseModelByProcessFlowList(List<FlowConfig> flowConfigs,String systemCode) {
		ResponseModel response = new ResponseModel();
		try {
			response.setSuccess(true);
			response.setMessage("Has "+flowConfigs.size()+" process of "+systemCode);
			response.setData(JSONUtil.listFromJson(JSONUtil.getJSONSerializer().serialize(flowConfigs)));
			
		} catch (Exception e) { response.setMessage(e.getMessage()); }
		
		return response;
	}

	@Override
	public ResponseModel findBySystemCodeOrderBySequenceDisplayAsc(String systemCode) {
		return getResponseModelByProcessFlowList(flowConfigRepository.findBySystemCodeOrderBySequenceDisplayAsc(systemCode),
				systemCode);
	}

}
