package com.mpg.itcane.flow.service;

import com.mpg.itcane.flow.model.RequestFlowModel;
import com.mpg.itcane.flow.model.ResponseModel;

public interface ProcessFlowService {

	public ResponseModel createProcess(String flowCode,String systemCode,String username,RequestFlowModel requestFlowModel);
	public ResponseModel approveProcess(String systemCode,Long processId,Long taskId,String username);
	public ResponseModel rejectProcess(String systemCode,Long processId,Long taskId,String username);
	public ResponseModel cancelProcess(String systemCode,Long processId,Long taskId,String username);
	public ResponseModel terminateProcess(String systemCode,Long processId);
	public ResponseModel findWaitingProcessByUserName(String username);
	public ResponseModel findWaitingProcessByUserNameAndFlowCode(String username,String flowCode);
	public ResponseModel findEndProcessByCreater(String creater);
	public ResponseModel findEndProcessAndTaskDetailByCreater(String creater);
	public ResponseModel findEndProcessByCreaterAndFlowCode(String creater,String flowCode);
	public ResponseModel findEndProcessAndTaskDetailByCreaterAndFlowCode(String creater,String flowCode);
	public ResponseModel getCurrentTask(Long processId);
	public ResponseModel findBySystemCodeOrderBySequenceDisplayAsc(String systemCode);
	
	
}
