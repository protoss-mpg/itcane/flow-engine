package com.mpg.itcane.flow.service.impl;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.mpg.itcane.flow.service.SendMailService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SendMailServiceImpl implements SendMailService {

    @Autowired
    RestTemplate restTemplate;
    
    @Autowired
    private Environment environment;
    
    
    private ResponseEntity<String> postWithMapParameter(Map<String, String> parameterMap, String urlParam) {
        String url = urlParam;
        log.info("url :{}", url);

        MediaType mediaType = new MediaType("application", "x-www-form-urlencoded", StandardCharsets.UTF_8);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);

        MultiValueMap<String, String> body = new LinkedMultiValueMap();
        if (parameterMap != null) {
            for (Entry<String, String> key : parameterMap.entrySet()) {
                if (!"parentId".equals(key.getKey()) && !"_csrf".equals(key.getKey())) {
                    body.add(key.getKey(), parameterMap.get(key));

                }


            }
        }


        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity(body, headers);

        FormHttpMessageConverter converter = new FormHttpMessageConverter();
        converter.setSupportedMediaTypes(Arrays.asList(mediaType));

        restTemplate.getMessageConverters().add(converter);
        if(environment.getActiveProfiles().length > 0 &&
				!"test".equalsIgnoreCase(environment.getActiveProfiles()[0])){
        	return restTemplate.postForEntity(url, entity, String.class);
        }else {
        	return ResponseEntity.ok().build();
        }
        
    }


	@Override
	public ResponseEntity<String> sendMail(Map<String, String> parameterMap, String urlParam) {
		return postWithMapParameter(parameterMap,urlParam);
	}
}
