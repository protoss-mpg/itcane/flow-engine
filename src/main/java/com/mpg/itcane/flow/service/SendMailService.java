package com.mpg.itcane.flow.service;

import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface SendMailService {

	public ResponseEntity<String> sendMail(Map<String, String> parameterMap, String urlParam);
}
