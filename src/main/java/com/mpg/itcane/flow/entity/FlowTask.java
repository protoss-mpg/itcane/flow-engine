package com.mpg.itcane.flow.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class FlowTask {

	/**
	 * 
	 */
	/* Standard Set of field */
    private  @Id @GeneratedValue(strategy = GenerationType.TABLE) Long id;
    private @Version @JsonIgnore Long version;
    private @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") Timestamp createDate;
    private String createBy;
    private @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") Timestamp updateDate;
    private String updateBy;
    
    private Integer sequence;
    private String approverUserName;
    private String approverUserId;
    private String approverUserEmail;
    private String actionStateCode;
    private String actionStateName;
    private String taskStatus;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp assignTime;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp actionTime;

    @JsonIgnore 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "processFlow")
    private ProcessFlow processFlow;
    
}
