package com.mpg.itcane.flow.entity;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class ProcessFlow {

	/**
	 * 
	 */
	/* Standard Set of field */
    private  @Id @GeneratedValue(strategy = GenerationType.TABLE) Long id;
    private @Version @JsonIgnore Long version;
    private @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") Timestamp createDate;
    private String createBy;
    private @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") Timestamp updateDate;
    private String updateBy;
    
    private String flowCode;
    private String documentNumber;
    private String description;
    private String processStatus;
	private String creater;
	private String createrEmail;
	private String requester;
	private String requesterEmail;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "processFlow")
    private Set<FlowTask> tasks;

}
