package com.mpg.itcane.flow;

public class ApplicationConstant {
	
	private ApplicationConstant() {
		super();
	}
	
	public static final String PARAMETER_FLOW_SYSTEM_USE_FLOW = "FLW001";

	/* ERROR CODE */
	public static final String ERROR_CODE_PROCESS_FAIL     = "FLWERR001";
	public static final String ERROR_CODE_THROW_EXCEPTION  = "FLWERR002";
	public static final String ERROR_CODE_NO_DATA_FOUND    = "FLWERR003";
	public static final String ERROR_CODE_REQUIRE_DATA     = "FLWERR004";
	
	/* PROCESS STATUS */
	public static final String PROCESS_STATUS_CREATED      = "CREATED";
	public static final String PROCESS_STATUS_CANCEL       = "CANCEL";
	public static final String PROCESS_STATUS_REJECT       = "REJECT";
	public static final String PROCESS_STATUS_TERMINATE    = "TERMINATE";
	public static final String PROCESS_STATUS_COMPLETE     = "COMPLETE";
	
	/* TASK STATUS */
	public static final String TASK_STATUS_NOT_ARRIVE      = "NOT_ARRIVE";
	public static final String TASK_STATUS_WAIT            = "WAIT";
	public static final String TASK_STATUS_CANCEL          = "CANCEL";
	public static final String TASK_STATUS_REJECT          = "REJECT";
	public static final String TASK_STATUS_APPROVE         = "APPROVE";
	
	/* EVENT TYPE */
	public static final String USER_EVENT_TYPE_CANCEL      = "CANCEL";
	public static final String USER_EVENT_TYPE_REJECT      = "REJECT";
	public static final String USER_EVENT_TYPE_APPROVE     = "APPROVE";
	

	/* Other */
	public static final String FIELD_CREATE_BY_DEFAULT      = "API";
	public static final String EMAIL_PROVIDER_CODE          = "ITCANE1";
	public static final String PATH_DELIMITER          		= "/";
	
	public static final String FIX_ADDRESS_TO               = "addressTo";
	public static final String FIX_CC_ADDRESS               = "ccAddress";
	public static final String FIX_APPROVER_NAME            = "approverName";
	public static final String FIX_ACTION_STATE_CODE        = "actionStateCode";
	public static final String FIX_ACTION_STATE_NAME        = "actionStateName";
	public static final String FIX_FROM_ADDRESS             = "fromAddress";
	public static final String FIX_PROVIDER_CODE            = "providerCode";
	public static final String FIX_TEMPLATE_CODE            = "templateCode";
	public static final String FIX_PROCESS_DATA             = "processData";
	public static final String FIX_EMAIL_DATA               = "emailData";
	public static final String FIX_PROCESS_ID               = "processId";
	public static final String FIX_TASK_ID                  = "taskId";
	public static final String FIX_DESCRIPTION              = "description";
	public static final String FIX_DOCUMENT_NUMBER          = "documentNumber";
	public static final String FIX_FROM                     = "from";
	public static final String FIX_APPROVER_USER_ID         = "approverUserId";
	public static final String FIX_TASK_STATUS              = "taskStatus";
	public static final String FIX_PROCESS_DATA_TASK        = "processData.tasks";
	public static final String FIX_TASK_PROCESS_FLOW        = "Task @ProcessFlow =";
	public static final String FIX_IS_EMPTY                 = " is empty";
	public static final String FIX_NO_TASK_FOUND            = "No Task Found";
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
