package com.mpg.itcane.flow.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mpg.itcane.flow.model.ResponseModel;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Api("Initial Application Information")
public class ApplicationInformationController {

	@GetMapping("/applicationInform")
    public ResponseEntity<ResponseModel> applicationInform() {
		ResponseModel response = new ResponseModel();
		response.setSuccess(true);
		response.setErrorCode(null);
		response.setMessage("Template of Spring Boot Engine");
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        
		return ResponseEntity.ok().headers(headers).body(response);
	}
}
