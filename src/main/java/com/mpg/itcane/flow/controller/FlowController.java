package com.mpg.itcane.flow.controller;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mpg.itcane.flow.entity.ProcessFlow;
import com.mpg.itcane.flow.model.RequestFlowModel;
import com.mpg.itcane.flow.model.ResponseModel;
import com.mpg.itcane.flow.repository.FlowTaskRepository;
import com.mpg.itcane.flow.repository.ProcessFlowRepository;
import com.mpg.itcane.flow.service.ProcessFlowService;
import com.mpg.itcane.flow.util.JSONUtil;
import com.mpg.itcane.flow.util.RequestUtil;
import com.mpg.itcane.flow.util.ResponseUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@Api("Service Of Flow Engine")

public class FlowController {
	
	@Autowired
	ProcessFlowService processFlowService;
	
	@Autowired
	ProcessFlowRepository processFlowRepository;
	
	@Autowired
	FlowTaskRepository flowTaskRepository;
	
	@PostMapping("/createProcess/{systemCode}/{flowCode}")
	public ResponseEntity<ResponseModel> createProcess(HttpServletRequest request,
    		@ApiParam(value = "Ex. ITCANE", required = true) 
    			@PathVariable(value = "systemCode") String systemCode,
    		@ApiParam(value = "Ex. F001", required = true) 
    			@PathVariable(value = "flowCode") String flowCode,
    		@ApiParam("{	\"documentNumber\":\"FARM201910280001\",\"description\":\"test flow\",\"creater\":\"suriya\",\"createrEmail\":\"suriya@protossgroup.com\",\"requester\":\"suriya\",\"requesterEmail\":\"suriya@protossgroup.com\",\"details\":[{\"sequence\":1,\"usernameApprover\":\"sukunratr\",\"roleFlow\":\"VRF\",\"userId\":\"1234567\",\"email\":\"sukunrat_r@protossgroup.com\"},{\"sequence\":2,\"usernameApprover\":\"suriyae\",\"roleFlow\":\"APR\",\"userId\":\"7654321\",\"email\":\"suriya_e@protossgroup.com\"} ]}")
    		    @RequestBody RequestFlowModel requestFlowModel) {
		
		ResponseModel response = processFlowService.createProcess(
				flowCode,
				systemCode,         
				RequestUtil.getUsername(request),
				requestFlowModel);
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        
		return ResponseEntity.created(URI.create(""))
				.headers(headers)
				.body(response);
	}
	
	
	@GetMapping("/process/{processId}")
	@ApiOperation("Check Process Status")
    public ResponseEntity<ResponseModel> findProcessById(@PathVariable(value = "processId") Long processId) {
		ResponseModel response = new ResponseModel();
		
		ProcessFlow processFlow;
		try {
			processFlow = processFlowRepository.getOne(processId);
			response.setSuccess(true);
			response.setMessage("Found data id="+processFlow.getId());
			response.setData(JSONUtil.mapFromJson(JSONUtil.getJSONSerializer().include("tasks").serialize(processFlow)) );
		} catch (Exception e) {
			log.error(e.getMessage());
			response.setSuccess(false);
			response.setMessage(e.getMessage());
		}
		return ResponseUtil.getResponseOk().body(response);
	}
	
	@GetMapping(value="/processAsEntity/{processId}",produces = "application/json; charset=utf-8")
	@ApiOperation("Check Process Status As Entity")
    public String findProcessByIdAsEntity(@PathVariable(value = "processId") Long processId) {
		return (JSONUtil.getJSONSerializer().include("tasks").serialize(processFlowRepository.getOne(processId)));
	}
	

	
	@GetMapping("/sendActionToFlow/{systemCode}/{processId}/{taskId}/{actionType}")
	@ApiOperation("Send action approve , reject and cancel")
    public ResponseEntity<ResponseModel> sendActionToFlow(
    		@PathVariable(value = "systemCode") String systemCode,
    		@PathVariable(value = "processId") Long processId,
    		@PathVariable(value = "taskId") Long taskId,
    		@ApiParam(value = "ACTION = { APPROVE | REJECT | CANCEL }", required = true) 
    			@PathVariable(value = "actionType") String actionType,
    		@RequestParam(value = "username") String username
    		) {
    	
    	ResponseModel response = null;
    	switch(actionType){
    		case "APPROVE" :
    			response = processFlowService.approveProcess(systemCode,processId, taskId, username);
    			break;
    		case "REJECT" :
    			response = processFlowService.rejectProcess(systemCode,processId, taskId, username);
    			break;
    		case "CANCEL" :
    			response = processFlowService.cancelProcess(systemCode,processId, taskId, username);
    			break;
    		default:
    			response = new ResponseModel();
    			break;
    	}
		return ResponseUtil.getResponseOk().body(response);
	}

	@GetMapping("/terminateProcess/{systemCode}/{processId}")
	@ApiOperation("Terminate Process")
    public ResponseEntity<ResponseModel> terminateProcess(
    		@ApiParam(value = "Ex. ITCANE", required = true) 
    			@PathVariable(value = "systemCode") String systemCode,
    		@PathVariable(value = "processId") Long processId) {
		ResponseModel response = processFlowService.terminateProcess(systemCode,processId);
		return ResponseUtil.getResponseOk().body(response);
	}
	
	
	@GetMapping("/findWaitingProcessByUserName/{username}")
	@ApiOperation("Find Waiting Process By User Name")
    public ResponseEntity<ResponseModel> findWaitingProcessByUserName(@PathVariable(value = "username") String username) {
		ResponseModel response = processFlowService.findWaitingProcessByUserName(username);
		return ResponseUtil.getResponseOk().body(response);
	}
	
	@GetMapping("/findWaitingProcessByUserNameAndFlowCode/{username}/{flowCode}")
	@ApiOperation("Find Waiting Process By User Name")
    public ResponseEntity<ResponseModel> findWaitingProcessByUserNameAndFlowCode(@PathVariable(value = "username") String username,@PathVariable(value = "flowCode") String flowCode) {
		ResponseModel response = processFlowService.findWaitingProcessByUserNameAndFlowCode(username, flowCode);
		return ResponseUtil.getResponseOk().body(response);
	}
	
	
	@GetMapping("/getCurrentTask/{processId}")
	@ApiOperation("Get Current Task By Process ID")
    public ResponseEntity<ResponseModel> getCurrentTask(@PathVariable(value = "processId") Long processId) {
		ResponseModel response = processFlowService.getCurrentTask(processId);
		return ResponseUtil.getResponseOk().body(response);
	}
	
	@GetMapping("/findEndProcessByCreater/{creater}")
	@ApiOperation("Find End Process By Creater")
    public ResponseEntity<ResponseModel> findEndProcessByCreater(@PathVariable(value = "creater") String creater) {
		ResponseModel response = processFlowService.findEndProcessByCreater(creater);
		return ResponseUtil.getResponseOk().body(response);
	}
	
	@GetMapping("/findEndProcessAndTaskDetailByCreater/{creater}")
	@ApiOperation("Find End Process And Task Detail By Creater")
    public ResponseEntity<ResponseModel> findEndProcessAndTaskDetailByCreater(@PathVariable(value = "creater") String creater) {
		ResponseModel response = processFlowService.findEndProcessAndTaskDetailByCreater(creater);
		return ResponseUtil.getResponseOk().body(response);
	}
	
	@GetMapping("/findEndProcessByCreaterAndFlowCode/{creater}/{flowCode}")
	@ApiOperation("Find End Process By Creater And Flow Code")
    public ResponseEntity<ResponseModel> findEndProcessByCreaterAndFlowCode(@PathVariable(value = "creater") String creater,@PathVariable(value = "flowCode") String flowCode) {
		ResponseModel response = processFlowService.findEndProcessByCreaterAndFlowCode(creater,flowCode);
		return ResponseUtil.getResponseOk().body(response);
	}
	
	@GetMapping("/findEndProcessAndTaskDetailByCreaterAndFlowCode/{creater}/{flowCode}")
	@ApiOperation("Find End Process And Task Detail By Creater And FlowCode")
    public ResponseEntity<ResponseModel> findEndProcessAndTaskDetailByCreaterAndFlowCode(@PathVariable(value = "creater") String creater,@PathVariable(value = "flowCode") String flowCode) {
		ResponseModel response = processFlowService.findEndProcessAndTaskDetailByCreaterAndFlowCode(creater,flowCode);
		return ResponseUtil.getResponseOk().body(response);
	}
	
	@GetMapping("/findFlowConfigBySystemCodeOrderBySequenceDisplayAsc/{systemCode}")
	@ApiOperation("Find Flow Config By SystemCode And OrderBy Sequence Display")
    public ResponseEntity<ResponseModel> findFlowConfigBySystemCodeOrderBySequenceDisplayAsc(@PathVariable(value = "systemCode") String systemCode) {
		ResponseModel response = processFlowService.findBySystemCodeOrderBySequenceDisplayAsc(systemCode);
		return ResponseUtil.getResponseOk().body(response);
	}
	
}
