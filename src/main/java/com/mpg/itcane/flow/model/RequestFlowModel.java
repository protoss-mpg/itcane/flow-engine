package com.mpg.itcane.flow.model;

import java.util.Set;

import lombok.Data;

@Data
public class RequestFlowModel {

	private String documentNumber;
	private String description;
	private String creater;
	private String createrEmail;
	private String requester;
	private String requesterEmail;
	private Set<RequestFlowDetailModel> details ;
}
