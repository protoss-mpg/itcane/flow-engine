package com.mpg.itcane.flow.model;

import lombok.Data;

@Data
public class RequestFlowDetailModel {


	private Integer sequence;
	private String roleFlow;
	private String usernameApprover;
	private String userId;
	private String email;
	
}
