package com.mpg.itcane.flow.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mpg.itcane.flow.entity.Parameter;

public interface ParameterRepository extends JpaSpecificationExecutor<Parameter>, JpaRepository<Parameter, Long> {
}
