package com.mpg.itcane.flow.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mpg.itcane.flow.entity.FlowConfigDetail;




public interface FlowConfigDetailRepository extends JpaSpecificationExecutor<FlowConfigDetail>, JpaRepository<FlowConfigDetail, Long>, PagingAndSortingRepository<FlowConfigDetail, Long>  {


}
