package com.mpg.itcane.flow.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;

import com.mpg.itcane.flow.entity.FlowConfig;


public interface FlowConfigRepository extends JpaSpecificationExecutor<FlowConfig>, JpaRepository<FlowConfig, Long> {
	FlowConfig findByCodeAndSystemCode(@Param("code") String code,@Param("systemCode") String systemCode);
	List<FlowConfig> findBySystemCodeOrderBySequenceDisplayAsc(@Param("systemCode") String systemCode);
}
