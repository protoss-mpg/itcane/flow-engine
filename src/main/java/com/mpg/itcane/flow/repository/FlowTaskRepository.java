package com.mpg.itcane.flow.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.mpg.itcane.flow.entity.FlowTask;




public interface FlowTaskRepository extends JpaSpecificationExecutor<FlowTask>, JpaRepository<FlowTask, Long>, PagingAndSortingRepository<FlowTask, Long>  {

	@Query("select DISTINCT u from FlowTask u left join u.processFlow r where r.processStatus in :processStatus and lower(u.taskStatus) like concat('%',lower(:taskStatus)) and lower(u.approverUserName) like concat('%',lower(:approverUserName)) order by u.assignTime")
	List<FlowTask> findByTaskStatusIgnoreCaseContainingAndApproverUserNameAndProcessFlowStatus( @Param("taskStatus") String taskStatus, @Param("approverUserName") String approverUserName, @Param("processStatus") String processStatus );

	@Query("select DISTINCT u from FlowTask u left join u.processFlow r where r.processStatus in :processStatus and lower(u.taskStatus) like concat('%',lower(:taskStatus)) and lower(u.approverUserName) like concat('%',lower(:approverUserName)) and r.flowCode = :flowCode order by u.assignTime")
	List<FlowTask> findByTaskStatusIgnoreCaseContainingAndApproverUserNameAndProcessFlowStatusAndFlowCode( @Param("taskStatus") String taskStatus, @Param("approverUserName") String approverUserName, @Param("processStatus") String processStatus, @Param("flowCode") String flowCode );

	
}
