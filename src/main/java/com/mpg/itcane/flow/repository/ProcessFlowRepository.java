package com.mpg.itcane.flow.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.mpg.itcane.flow.entity.ProcessFlow;




public interface ProcessFlowRepository extends JpaSpecificationExecutor<ProcessFlow>, JpaRepository<ProcessFlow, Long>, PagingAndSortingRepository<ProcessFlow, Long>  {

	List<ProcessFlow> findByCreaterAndProcessStatusNotIn(@Param("creater") String creater,@Param("processStatus") List<String> processStatus);
	List<ProcessFlow> findByRequesterAndProcessStatusNotIn(@Param("requester") String requester,@Param("processStatus") List<String> processStatus);
	List<ProcessFlow> findByCreaterAndFlowCodeAndProcessStatusNotIn(@Param("creater") String creater,@Param("flowCode") String flowCode,@Param("processStatus") List<String> processStatus);
	List<ProcessFlow> findByRequesterAndFlowCodeAndProcessStatusNotIn(@Param("requester") String requester,@Param("flowCode") String flowCode,@Param("processStatus") List<String> processStatus);
}
