package com.mpg.itcane.flow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ApplicationInformationControllerTest.class, EngineApplicationTest.class, FlowContinueTest.class })
public class AllTests {

}
