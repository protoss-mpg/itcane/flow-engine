package com.mpg.itcane.flow;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.mpg.itcane.flow.repository.FlowConfigDetailRepository;
import com.mpg.itcane.flow.repository.FlowConfigRepository;
import com.mpg.itcane.flow.repository.FlowTaskRepository;
import com.mpg.itcane.flow.repository.ParameterDetailRepository;
import com.mpg.itcane.flow.repository.ParameterRepository;
import com.mpg.itcane.flow.util.JSONUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class EngineApplicationTest {
	
	 @Autowired
	 ParameterRepository parameterRepository;

	 @Autowired
	 ParameterDetailRepository parameterDetailRepository;
	 
	 @Autowired
	 FlowConfigRepository flowConfigRepository;
	 
	 @Autowired
	 FlowConfigDetailRepository flowConfigDetailRepository;
	 
	 @Autowired
	 FlowTaskRepository flowTaskRepository;

	 @Autowired
	 private WebApplicationContext wac;
	 private MockMvc mockMvc;
	 
	 @Before
	 public void setUp() {
	        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	 }

	 @After
	 public void tearDown() {
	 }

	 private String sampleCreateFlow() throws Exception{
		 /* Request Setting */
	    	String systemCode = "ITCANE";
	    	String flowCode   = "F001";
	    	Map<String,Object> requestJsonBodyMap = new HashMap();
	    	
	    	List<Map<String,Object>> testDetailLs = new ArrayList();
	    	Map<String,Object> task1 = new HashMap();
	    	task1.put("sequence", 1);
	    	task1.put("usernameApprover", "sakunrat_r");
	    	task1.put("roleFlow", "VRF");
	    	task1.put("userId", "1234567");
	    	task1.put("email", "sakunrat_r@gmail.com");
	    	testDetailLs.add(task1);

	    	Map<String,Object> task2 = new HashMap();
	    	task2.put("sequence", 2);
	    	task2.put("usernameApprover", "suriya_e");
	    	task2.put("roleFlow", "APR");
	    	task2.put("userId", "7654321");
	    	task2.put("email", "suriya_e@gmail.com");
	    	testDetailLs.add(task2);
	    	

	    	requestJsonBodyMap.put("description", "test flow");
	    	requestJsonBodyMap.put("creater", "nipitphol_e");
	    	requestJsonBodyMap.put("createrEmail", "nipitphol_e@gmail.com");
	    	requestJsonBodyMap.put("requester", "nakarin_e");
	    	requestJsonBodyMap.put("requesterEmail", "nakarin_e@gmail.com");
	    	requestJsonBodyMap.put("details", testDetailLs);
	    	String requestJsonBody = JSONUtil.getJSONSerializer().include("details").serialize(requestJsonBodyMap) ;
	    	
	       
	    	/* Response Setting */
	    	/* Set Task */
	    	List<Map<String,Object>> responseJsonDataDetailLs = new ArrayList();
	    	Map<String,Object> responseJsonDataDetailMap1 = new HashMap();
	    	responseJsonDataDetailMap1.put("sequence", 1);
	    	responseJsonDataDetailMap1.put("approverUserName", "sakunrat_r");
	    	responseJsonDataDetailMap1.put("actionStateCode", "VRF");
	    	responseJsonDataDetailMap1.put("actionStateName", "Verify");
	    	responseJsonDataDetailMap1.put("taskStatus", "WAIT");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap1);
	    	
	    	
	    	Map<String,Object> responseJsonDataDetailMap2 = new HashMap();
	    	responseJsonDataDetailMap2.put("sequence", 2);
	    	responseJsonDataDetailMap2.put("approverUserName", "suriya_e");
	    	responseJsonDataDetailMap2.put("actionStateCode", "APR");
	    	responseJsonDataDetailMap2.put("actionStateName", "Approve");
	    	responseJsonDataDetailMap2.put("taskStatus", "NOT_ARRIVE");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap2);
	    	
	    	/* Set Process */
	    	Map<String,Object> responseJsonDataHeaderMap = new HashMap();
	    	responseJsonDataHeaderMap.put("flowCode", "F001");
	    	responseJsonDataHeaderMap.put("description", "test flow");
	    	responseJsonDataHeaderMap.put("processStatus", "CREATED");
	    	responseJsonDataHeaderMap.put("requester", "nakarin_e");
	    	responseJsonDataHeaderMap.put("tasks", responseJsonDataDetailLs);
	    	
	    	/* Set Email */
	    	Map<String,Object> responseJsonDataEmailMap = new HashMap();
	    	responseJsonDataEmailMap.put("providerCode", "ITCANE1");
	    	responseJsonDataEmailMap.put("actionStateName", "Verify");
	    	responseJsonDataEmailMap.put("actionStateCode", "VRF");
	    	responseJsonDataEmailMap.put("approverName", "sakunrat_r");
	    	responseJsonDataEmailMap.put("approverUserId", "1234567");
	    	responseJsonDataEmailMap.put("taskStatus", "WAIT");
	    	responseJsonDataEmailMap.put("templateCode", "ITCANE1_CREATED");	
	    	responseJsonDataEmailMap.put("from", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("fromAddress", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("addressTo", "sakunrat_r@gmail.com");
	    	responseJsonDataEmailMap.put("ccAddress", "nakarin_e@gmail.com");
         
	    	/* Set Main Structure */
	    	Map<String,Object> responseJsonDataMap = new HashMap();
	    	responseJsonDataMap.put("processData", responseJsonDataHeaderMap);
	    	responseJsonDataMap.put("emailData", responseJsonDataEmailMap);
	    	
	    	Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("data", responseJsonDataMap);
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	        
	    	return mockMvc.perform(post("/createProcess/"+systemCode+"/"+flowCode)
	        		.contentType(MediaType.APPLICATION_JSON)
	        		.content(requestJsonBody)
	        		)
	                .andExpect(status().isCreated())
	                .andExpect(content().json(responseJsonBody))
	                .andReturn().getResponse().getContentAsString()
	                ;
	 }
	 
	 @Test
	 public void createFlow() throws Exception {
		 	/* Call Simple Create Flow */
		    sampleCreateFlow();
	  }
	 
	 @Test
	 public void approveFlow() throws Exception  {

	    	String systemCode = "ITCANE";
		 
		 	Map<String,Object> requestJsonBodyMap = new HashMap();
	    	
	    	List<Map<String,Object>> testDetailLs = new ArrayList();
	    	Map<String,Object> task1 = new HashMap();
	    	task1.put("sequence", 1);
	    	task1.put("usernameApprover", "sakunrat_r");
	    	task1.put("roleFlow", "VRF");
	    	task1.put("userId", "1234567");
	    	task1.put("email", "sakunrat_r@gmail.com");
	    	testDetailLs.add(task1);

	    	Map<String,Object> task2 = new HashMap();
	    	task2.put("sequence", 2);
	    	task2.put("usernameApprover", "suriya_e");
	    	task2.put("roleFlow", "APR");
	    	task2.put("userId", "7654321");
	    	task2.put("email", "suriya_e@gmail.com");
	    	testDetailLs.add(task2);
	    	

	    	requestJsonBodyMap.put("description", "test flow");
	    	requestJsonBodyMap.put("creater", "nipitphol_e");
	    	requestJsonBodyMap.put("createrEmail", "nipitphol_e@gmail.com");
	    	requestJsonBodyMap.put("requester", "nakarin_e");
	    	requestJsonBodyMap.put("requesterEmail", "nakarin_e@gmail.com");
	    	requestJsonBodyMap.put("details", testDetailLs);
	    	JSONUtil.getJSONSerializer().include("details").serialize(requestJsonBodyMap) ;
	    	
	       
	    	/* Response Setting */
	    	/* Set Task */
	    	List<Map<String,Object>> responseJsonDataDetailLs = new ArrayList();
	    	Map<String,Object> responseJsonDataDetailMap1 = new HashMap();
	    	responseJsonDataDetailMap1.put("sequence", 1);
	    	responseJsonDataDetailMap1.put("approverUserName", "sakunrat_r");
	    	responseJsonDataDetailMap1.put("actionStateCode", "VRF");
	    	responseJsonDataDetailMap1.put("actionStateName", "Verify");
	    	responseJsonDataDetailMap1.put("taskStatus", "APPROVE");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap1);
	    	
	    	
	    	Map<String,Object> responseJsonDataDetailMap2 = new HashMap();
	    	responseJsonDataDetailMap2.put("sequence", 2);
	    	responseJsonDataDetailMap2.put("approverUserName", "suriya_e");
	    	responseJsonDataDetailMap2.put("actionStateCode", "APR");
	    	responseJsonDataDetailMap2.put("actionStateName", "Approve");
	    	responseJsonDataDetailMap2.put("taskStatus", "WAIT");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap2);
	    	
	    	/* Set Process */
	    	Map<String,Object> responseJsonDataHeaderMap = new HashMap();
	    	responseJsonDataHeaderMap.put("flowCode", "F001");
	    	responseJsonDataHeaderMap.put("description", "test flow");
	    	responseJsonDataHeaderMap.put("processStatus", "CREATED");
	    	responseJsonDataHeaderMap.put("requester", "nakarin_e");
	    	responseJsonDataHeaderMap.put("tasks", responseJsonDataDetailLs);
	    	
	    	/* Set Email */
	    	Map<String,Object> responseJsonDataEmailMap = new HashMap();
	    	responseJsonDataEmailMap.put("providerCode", "ITCANE1");
	    	responseJsonDataEmailMap.put("actionStateName", "Approve");
	    	responseJsonDataEmailMap.put("actionStateCode", "APR");
	    	responseJsonDataEmailMap.put("approverName", "suriya_e");
	    	responseJsonDataEmailMap.put("approverUserId", "7654321");
	    	responseJsonDataEmailMap.put("taskStatus", "WAIT");
	    	responseJsonDataEmailMap.put("templateCode", "ITCANE1_CREATED");	
	    	responseJsonDataEmailMap.put("from", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("fromAddress", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("addressTo", "suriya_e@gmail.com");
	    	responseJsonDataEmailMap.put("ccAddress", "nakarin_e@gmail.com");
	    	
	    	
		 	/* Set Main Structure */
	    	Map<String,Object> responseJsonDataMap = new HashMap();
	    	responseJsonDataMap.put("processData", responseJsonDataHeaderMap);
	    	responseJsonDataMap.put("emailData", responseJsonDataEmailMap);
	    	
		    Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Approve successful.");
	    	responseJsonBodyMap.put("data", responseJsonDataMap);
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	        
	    	/* Create Flow */
	        String jsonCreateFlow  = sampleCreateFlow();
	        
	        /* Get Process ID */
			Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
			
			/* Get Current Task of Process */
			String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
			Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
			
			mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void rejectFlow() throws Exception  {

	    	String systemCode = "ITCANE";
		 
		 	Map<String,Object> requestJsonBodyMap = new HashMap();
	    	
	    	List<Map<String,Object>> testDetailLs = new ArrayList();
	    	Map<String,Object> task1 = new HashMap();
	    	task1.put("sequence", 1);
	    	task1.put("usernameApprover", "sakunrat_r");
	    	task1.put("roleFlow", "VRF");
	    	task1.put("userId", "1234567");
	    	task1.put("email", "sakunrat_r@gmail.com");
	    	testDetailLs.add(task1);

	    	Map<String,Object> task2 = new HashMap();
	    	task2.put("sequence", 2);
	    	task2.put("usernameApprover", "suriya_e");
	    	task2.put("roleFlow", "APR");
	    	task2.put("userId", "7654321");
	    	task2.put("email", "suriya_e@gmail.com");
	    	testDetailLs.add(task2);
	    	

	    	requestJsonBodyMap.put("description", "test flow");
	    	requestJsonBodyMap.put("creater", "nipitphol_e");
	    	requestJsonBodyMap.put("createrEmail", "nipitphol_e@gmail.com");
	    	requestJsonBodyMap.put("requester", "nakarin_e");
	    	requestJsonBodyMap.put("requesterEmail", "nakarin_e@gmail.com");
	    	requestJsonBodyMap.put("details", testDetailLs);
	    	JSONUtil.getJSONSerializer().include("details").serialize(requestJsonBodyMap) ;
	    	
	       
	    	/* Response Setting */
	    	/* Set Task */
	    	List<Map<String,Object>> responseJsonDataDetailLs = new ArrayList();
	    	Map<String,Object> responseJsonDataDetailMap1 = new HashMap();
	    	responseJsonDataDetailMap1.put("sequence", 1);
	    	responseJsonDataDetailMap1.put("approverUserName", "sakunrat_r");
	    	responseJsonDataDetailMap1.put("actionStateCode", "VRF");
	    	responseJsonDataDetailMap1.put("actionStateName", "Verify");
	    	responseJsonDataDetailMap1.put("taskStatus", "REJECT");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap1);
	    	
	    	
	    	Map<String,Object> responseJsonDataDetailMap2 = new HashMap();
	    	responseJsonDataDetailMap2.put("sequence", 2);
	    	responseJsonDataDetailMap2.put("approverUserName", "suriya_e");
	    	responseJsonDataDetailMap2.put("actionStateCode", "APR");
	    	responseJsonDataDetailMap2.put("actionStateName", "Approve");
	    	responseJsonDataDetailMap2.put("taskStatus", "NOT_ARRIVE");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap2);
	    	
	    	/* Set Process */
	    	Map<String,Object> responseJsonDataHeaderMap = new HashMap();
	    	responseJsonDataHeaderMap.put("flowCode", "F001");
	    	responseJsonDataHeaderMap.put("description", "test flow");
	    	responseJsonDataHeaderMap.put("processStatus", "REJECT");
	    	responseJsonDataHeaderMap.put("requester", "nakarin_e");
	    	responseJsonDataHeaderMap.put("tasks", responseJsonDataDetailLs);
	    	
	    	/* Set Email */
	    	Map<String,Object> responseJsonDataEmailMap = new HashMap();
	    	responseJsonDataEmailMap.put("providerCode", "ITCANE1");
	    	responseJsonDataEmailMap.put("actionStateName", "Verify");
	    	responseJsonDataEmailMap.put("actionStateCode", "VRF");
	    	responseJsonDataEmailMap.put("approverName", "sakunrat_r");
	    	responseJsonDataEmailMap.put("approverUserId", "1234567");
	    	responseJsonDataEmailMap.put("taskStatus", "REJECT");
	    	responseJsonDataEmailMap.put("templateCode", "ITCANE1_REJECT");	
	    	responseJsonDataEmailMap.put("from", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("fromAddress", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("addressTo", "nakarin_e@gmail.com");
	    	responseJsonDataEmailMap.put("ccAddress", "nipitphol_e@gmail.com");
	    	
	    	
		 	/* Set Main Structure */
	    	Map<String,Object> responseJsonDataMap = new HashMap();
	    	responseJsonDataMap.put("processData", responseJsonDataHeaderMap);
	    	responseJsonDataMap.put("emailData", responseJsonDataEmailMap);
	    	
		    Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Reject successful.");
	    	responseJsonBodyMap.put("data", responseJsonDataMap);
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	        
	    	/* Create Flow */
	        String jsonCreateFlow  = sampleCreateFlow();
	        
	        /* Get Process ID */
			Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
			
			/* Get Current Task of Process */
			String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
			Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
			
			mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_REJECT)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void cancelFlow() throws Exception  {

	    	String systemCode = "ITCANE";
		 
		 	Map<String,Object> requestJsonBodyMap = new HashMap();
	    	
	    	List<Map<String,Object>> testDetailLs = new ArrayList();
	    	Map<String,Object> task1 = new HashMap();
	    	task1.put("sequence", 1);
	    	task1.put("usernameApprover", "sakunrat_r");
	    	task1.put("roleFlow", "VRF");
	    	task1.put("userId", "1234567");
	    	task1.put("email", "sakunrat_r@gmail.com");
	    	testDetailLs.add(task1);

	    	Map<String,Object> task2 = new HashMap();
	    	task2.put("sequence", 2);
	    	task2.put("usernameApprover", "suriya_e");
	    	task2.put("roleFlow", "APR");
	    	task2.put("userId", "7654321");
	    	task2.put("email", "suriya_e@gmail.com");
	    	testDetailLs.add(task2);
	    	

	    	requestJsonBodyMap.put("description", "test flow");
	    	requestJsonBodyMap.put("creater", "nipitphol_e");
	    	requestJsonBodyMap.put("createrEmail", "nipitphol_e@gmail.com");
	    	requestJsonBodyMap.put("requester", "nakarin_e");
	    	requestJsonBodyMap.put("requesterEmail", "nakarin_e@gmail.com");
	    	requestJsonBodyMap.put("details", testDetailLs);
	    	JSONUtil.getJSONSerializer().include("details").serialize(requestJsonBodyMap) ;
	    	
	       
	    	/* Response Setting */
	    	/* Set Task */
	    	List<Map<String,Object>> responseJsonDataDetailLs = new ArrayList();
	    	Map<String,Object> responseJsonDataDetailMap1 = new HashMap();
	    	responseJsonDataDetailMap1.put("sequence", 1);
	    	responseJsonDataDetailMap1.put("approverUserName", "sakunrat_r");
	    	responseJsonDataDetailMap1.put("actionStateCode", "VRF");
	    	responseJsonDataDetailMap1.put("actionStateName", "Verify");
	    	responseJsonDataDetailMap1.put("taskStatus", "WAIT");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap1);
	    	
	    	
	    	Map<String,Object> responseJsonDataDetailMap2 = new HashMap();
	    	responseJsonDataDetailMap2.put("sequence", 2);
	    	responseJsonDataDetailMap2.put("approverUserName", "suriya_e");
	    	responseJsonDataDetailMap2.put("actionStateCode", "APR");
	    	responseJsonDataDetailMap2.put("actionStateName", "Approve");
	    	responseJsonDataDetailMap2.put("taskStatus", "NOT_ARRIVE");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap2);
	    	
	    	/* Set Process */
	    	Map<String,Object> responseJsonDataHeaderMap = new HashMap();
	    	responseJsonDataHeaderMap.put("flowCode", "F001");
	    	responseJsonDataHeaderMap.put("description", "test flow");
	    	responseJsonDataHeaderMap.put("processStatus", "CANCEL");
	    	responseJsonDataHeaderMap.put("requester", "nakarin_e");
	    	responseJsonDataHeaderMap.put("tasks", responseJsonDataDetailLs);
	    	
	    	/* Set Email */
	    	Map<String,Object> responseJsonDataEmailMap = new HashMap();
	    	responseJsonDataEmailMap.put("providerCode", "ITCANE1");
	    	responseJsonDataEmailMap.put("actionStateName", "Verify");
	    	responseJsonDataEmailMap.put("actionStateCode", "VRF");
	    	responseJsonDataEmailMap.put("approverName", "sakunrat_r");
	    	responseJsonDataEmailMap.put("approverUserId", "1234567");
	    	responseJsonDataEmailMap.put("taskStatus", "WAIT");
	    	responseJsonDataEmailMap.put("templateCode", "ITCANE1_CANCEL");	
	    	responseJsonDataEmailMap.put("from", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("fromAddress", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("addressTo", "sakunrat_r@gmail.com");
	    	responseJsonDataEmailMap.put("ccAddress", "nipitphol_e@gmail.com,nakarin_e@gmail.com");
	    	
	    	
		 	/* Set Main Structure */
	    	Map<String,Object> responseJsonDataMap = new HashMap();
	    	responseJsonDataMap.put("processData", responseJsonDataHeaderMap);
	    	responseJsonDataMap.put("emailData", responseJsonDataEmailMap);
	    	
		    Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Cancel successful.");
	    	responseJsonBodyMap.put("data", responseJsonDataMap);
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	        
	    	/* Create Flow */
	        String jsonCreateFlow  = sampleCreateFlow();
	        
	        /* Get Process ID */
			Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
			
			/* Get Current Task of Process */
			String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
			Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
			
			mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_CANCEL)
					.param("username", "nakarin_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void terminateFlow() throws Exception  {

	    	String systemCode = "ITCANE";
		 
		 	Map<String,Object> requestJsonBodyMap = new HashMap();
	    	
		 	List<Map<String,Object>> testDetailLs = new ArrayList();
	    	Map<String,Object> task1 = new HashMap();
	    	task1.put("sequence", 1);
	    	task1.put("usernameApprover", "sakunrat_r");
	    	task1.put("roleFlow", "VRF");
	    	task1.put("userId", "1234567");
	    	task1.put("email", "sakunrat_r@gmail.com");
	    	testDetailLs.add(task1);

	    	Map<String,Object> task2 = new HashMap();
	    	task2.put("sequence", 2);
	    	task2.put("usernameApprover", "suriya_e");
	    	task2.put("roleFlow", "APR");
	    	task2.put("userId", "7654321");
	    	task2.put("email", "suriya_e@gmail.com");
	    	testDetailLs.add(task2);
	    	

	    	requestJsonBodyMap.put("description", "test flow");
	    	requestJsonBodyMap.put("creater", "nipitphol_e");
	    	requestJsonBodyMap.put("createrEmail", "nipitphol_e@gmail.com");
	    	requestJsonBodyMap.put("requester", "nakarin_e");
	    	requestJsonBodyMap.put("requesterEmail", "nakarin_e@gmail.com");
	    	requestJsonBodyMap.put("details", testDetailLs);
	    	JSONUtil.getJSONSerializer().include("details").serialize(requestJsonBodyMap) ;
	    	
	       
	    	/* Response Setting */
	    	/* Set Task */
	    	List<Map<String,Object>> responseJsonDataDetailLs = new ArrayList();
	    	Map<String,Object> responseJsonDataDetailMap1 = new HashMap();
	    	responseJsonDataDetailMap1.put("sequence", 1);
	    	responseJsonDataDetailMap1.put("approverUserName", "sakunrat_r");
	    	responseJsonDataDetailMap1.put("actionStateCode", "VRF");
	    	responseJsonDataDetailMap1.put("actionStateName", "Verify");
	    	responseJsonDataDetailMap1.put("taskStatus", "WAIT");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap1);
	    	
	    	
	    	Map<String,Object> responseJsonDataDetailMap2 = new HashMap();
	    	responseJsonDataDetailMap2.put("sequence", 2);
	    	responseJsonDataDetailMap2.put("approverUserName", "suriya_e");
	    	responseJsonDataDetailMap2.put("actionStateCode", "APR");
	    	responseJsonDataDetailMap2.put("actionStateName", "Approve");
	    	responseJsonDataDetailMap2.put("taskStatus", "NOT_ARRIVE");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap2);
	    	
	    	/* Set Process */
	    	Map<String,Object> responseJsonDataHeaderMap = new HashMap();
	    	responseJsonDataHeaderMap.put("flowCode", "F001");
	    	responseJsonDataHeaderMap.put("description", "test flow");
	    	responseJsonDataHeaderMap.put("processStatus", "TERMINATE");
	    	responseJsonDataHeaderMap.put("requester", "nakarin_e");
	    	responseJsonDataHeaderMap.put("tasks", responseJsonDataDetailLs);
	    	
	    	/* Set Email */
	    	Map<String,Object> responseJsonDataEmailMap = new HashMap();
	    	responseJsonDataEmailMap.put("providerCode", "ITCANE1");
	    	responseJsonDataEmailMap.put("actionStateName", "Verify");
	    	responseJsonDataEmailMap.put("actionStateCode", "VRF");
	    	responseJsonDataEmailMap.put("approverName", "sakunrat_r");
	    	responseJsonDataEmailMap.put("approverUserId", "1234567");
	    	responseJsonDataEmailMap.put("taskStatus", "WAIT");
	    	responseJsonDataEmailMap.put("templateCode", "ITCANE1_TERMINATE");	
	    	responseJsonDataEmailMap.put("from", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("fromAddress", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("addressTo", "nakarin_e@gmail.com");
	    	responseJsonDataEmailMap.put("ccAddress", "nipitphol_e@gmail.com,sakunrat_r@gmail.com");
	    	
	    	
		 	/* Set Main Structure */
	    	Map<String,Object> responseJsonDataMap = new HashMap();
	    	responseJsonDataMap.put("processData", responseJsonDataHeaderMap);
	    	responseJsonDataMap.put("emailData", responseJsonDataEmailMap);
	    	
		    Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Terminate successful.");
	    	responseJsonBodyMap.put("data", responseJsonDataMap);
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	        
	        String jsonCreateFlow  = sampleCreateFlow();
	        
			Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
			mockMvc.perform(get("/terminateProcess/"+systemCode+"/"+processId))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void findWaitingProcessByUserNamePositiveCase1Process() throws Exception  {

		 	Map<String,Object> requestJsonBodyMap = new HashMap();
	    	
		 	List<Map<String,Object>> testDetailLs = new ArrayList();
	    	Map<String,Object> task1 = new HashMap();
	    	task1.put("sequence", 1);
	    	task1.put("usernameApprover", "sakunrat_r");
	    	task1.put("roleFlow", "VRF");
	    	task1.put("userId", "1234567");
	    	task1.put("email", "sakunrat_r@gmail.com");
	    	testDetailLs.add(task1);

	    	Map<String,Object> task2 = new HashMap();
	    	task2.put("sequence", 2);
	    	task2.put("usernameApprover", "suriya_e");
	    	task2.put("roleFlow", "APR");
	    	task2.put("userId", "7654321");
	    	task2.put("email", "suriya_e@gmail.com");
	    	testDetailLs.add(task2);
	    	

	    	requestJsonBodyMap.put("description", "test flow");
	    	requestJsonBodyMap.put("creater", "nipitphol_e");
	    	requestJsonBodyMap.put("createrEmail", "nipitphol_e@gmail.com");
	    	requestJsonBodyMap.put("requester", "nakarin_e");
	    	requestJsonBodyMap.put("requesterEmail", "nakarin_e@gmail.com");
	    	requestJsonBodyMap.put("details", testDetailLs);
	    	JSONUtil.getJSONSerializer().include("details").serialize(requestJsonBodyMap) ;
	    	
	    	Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Has 1 Task Wait for user=sakunrat_r");
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	       
	    	
	        sampleCreateFlow();
			mockMvc.perform(get("/findWaitingProcessByUserName/"+"sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void findWaitingProcessByUserNamePositiveCase2Process() throws Exception  {

		 	Map<String,Object> requestJsonBodyMap = new HashMap();
	    	
		 	List<Map<String,Object>> testDetailLs = new ArrayList();
	    	Map<String,Object> task1 = new HashMap();
	    	task1.put("sequence", 1);
	    	task1.put("usernameApprover", "sakunrat_r");
	    	task1.put("roleFlow", "VRF");
	    	task1.put("userId", "1234567");
	    	task1.put("email", "sakunrat_r@gmail.com");
	    	testDetailLs.add(task1);

	    	Map<String,Object> task2 = new HashMap();
	    	task2.put("sequence", 2);
	    	task2.put("usernameApprover", "suriya_e");
	    	task2.put("roleFlow", "APR");
	    	task2.put("userId", "7654321");
	    	task2.put("email", "suriya_e@gmail.com");
	    	testDetailLs.add(task2);
	    	

	    	requestJsonBodyMap.put("description", "test flow");
	    	requestJsonBodyMap.put("creater", "nipitphol_e");
	    	requestJsonBodyMap.put("createrEmail", "nipitphol_e@gmail.com");
	    	requestJsonBodyMap.put("requester", "nakarin_e");
	    	requestJsonBodyMap.put("requesterEmail", "nakarin_e@gmail.com");
	    	requestJsonBodyMap.put("details", testDetailLs);
	    	JSONUtil.getJSONSerializer().include("details").serialize(requestJsonBodyMap) ;
	    	
	    	Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Has 2 Task Wait for user=sakunrat_r");
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	       
	    	/* Create Process 1 */
	        sampleCreateFlow();
	        
	    	/* Create Process 2 */
	        sampleCreateFlow();
	        
			mockMvc.perform(get("/findWaitingProcessByUserName/"+"sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void findWaitingProcessByUserNameEceptionCase1Process() throws Exception  {
			mockMvc.perform(get("/findWaitingProcessByUserName/"+"sakunrat_r"))
			        .andExpect(status().isOk());
	  }
	 
	 @Test
	 public void findWaitingProcessByUserNameAndFlowCodeEceptionCase1Process() throws Exception  {
			mockMvc.perform(get("/findWaitingProcessByUserNameAndFlowCode/"+"sakunrat_r/F001"))
			        .andExpect(status().isOk());
	  }
	 
	 @Test
	 public void findProcessById() throws Exception  {
	    	String jsonCreateFlow  = sampleCreateFlow();
			Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
			
			Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Found data id="+processId);
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	        
			mockMvc.perform(get("/process/"+processId))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 

	 
	 @Test
	 public void findProcessByIdException() throws Exception  {
	    	String jsonCreateFlow  = sampleCreateFlow();
			Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
			
			Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", false);
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	        
			mockMvc.perform(get("/process/"+processId+"0"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void processAsEntity() throws Exception  {
	    	String jsonCreateFlow  = sampleCreateFlow();
			Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
			
			mockMvc.perform(get("/processAsEntity/"+processId)
					.contentType(MediaType.APPLICATION_JSON)
					)
			        .andExpect(status().isOk())
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void findWaitingProcessByUserNameAndFlowCode() throws Exception  {

		 	Map<String,Object> requestJsonBodyMap = new HashMap();
	    	
		 	List<Map<String,Object>> testDetailLs = new ArrayList();
	    	Map<String,Object> task1 = new HashMap();
	    	task1.put("sequence", 1);
	    	task1.put("usernameApprover", "sakunrat_r");
	    	task1.put("roleFlow", "VRF");
	    	task1.put("userId", "1234567");
	    	task1.put("email", "sakunrat_r@gmail.com");
	    	testDetailLs.add(task1);

	    	Map<String,Object> task2 = new HashMap();
	    	task2.put("sequence", 2);
	    	task2.put("usernameApprover", "suriya_e");
	    	task2.put("roleFlow", "APR");
	    	task2.put("userId", "7654321");
	    	task2.put("email", "suriya_e@gmail.com");
	    	testDetailLs.add(task2);
	    	

	    	requestJsonBodyMap.put("description", "test flow");
	    	requestJsonBodyMap.put("creater", "nipitphol_e");
	    	requestJsonBodyMap.put("createrEmail", "nipitphol_e@gmail.com");
	    	requestJsonBodyMap.put("requester", "nakarin_e");
	    	requestJsonBodyMap.put("requesterEmail", "nakarin_e@gmail.com");
	    	requestJsonBodyMap.put("details", testDetailLs);
	    	JSONUtil.getJSONSerializer().include("details").serialize(requestJsonBodyMap) ;
	    	
	    	Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Has 1 Task Wait for user=sakunrat_r");
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	       
	    	
	        sampleCreateFlow();
			mockMvc.perform(get("/findWaitingProcessByUserNameAndFlowCode/"+"sakunrat_r"+ApplicationConstant.PATH_DELIMITER+"F001"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void sendOtherAction() throws Exception  {

	    	String systemCode = "ITCANE";
		 
		 	Map<String,Object> requestJsonBodyMap = new HashMap();
	    	
	    	List<Map<String,Object>> testDetailLs = new ArrayList();
	    	Map<String,Object> task1 = new HashMap();
	    	task1.put("sequence", 1);
	    	task1.put("usernameApprover", "sakunrat_r");
	    	task1.put("roleFlow", "VRF");
	    	task1.put("userId", "1234567");
	    	task1.put("email", "sakunrat_r@gmail.com");
	    	testDetailLs.add(task1);

	    	Map<String,Object> task2 = new HashMap();
	    	task2.put("sequence", 2);
	    	task2.put("usernameApprover", "suriya_e");
	    	task2.put("roleFlow", "APR");
	    	task2.put("userId", "7654321");
	    	task2.put("email", "suriya_e@gmail.com");
	    	testDetailLs.add(task2);
	    	

	    	requestJsonBodyMap.put("description", "test flow");
	    	requestJsonBodyMap.put("creater", "nipitphol_e");
	    	requestJsonBodyMap.put("createrEmail", "nipitphol_e@gmail.com");
	    	requestJsonBodyMap.put("requester", "nakarin_e");
	    	requestJsonBodyMap.put("requesterEmail", "nakarin_e@gmail.com");
	    	requestJsonBodyMap.put("details", testDetailLs);
	    	JSONUtil.getJSONSerializer().include("details").serialize(requestJsonBodyMap) ;
	    	
	       
	    	/* Response Setting */
	    	
		    Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", false);
	    	responseJsonBodyMap.put("message", "Process Fail");
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	        
	    	/* Create Flow */
	        String jsonCreateFlow  = sampleCreateFlow();
	        
	        /* Get Process ID */
			Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
			
			/* Get Current Task of Process */
			String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
			Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
			
			mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+"OTHER")
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void getCurrentTask() throws Exception  {
			mockMvc.perform(get("/getCurrentTask/45")) 
			.andExpect(status().isOk()) 
			.andExpect(content().json(
	        		"{"
	        		+ "\"success\":false,"
	        		+ "\"message\":\"Unable to find com.mpg.itcane.flow.entity.ProcessFlow with id 45\""
	        		+ "}"
	        		));
			
	  }
	 
}
