package com.mpg.itcane.flow;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.mpg.itcane.flow.repository.FlowConfigDetailRepository;
import com.mpg.itcane.flow.repository.FlowConfigRepository;
import com.mpg.itcane.flow.repository.FlowTaskRepository;
import com.mpg.itcane.flow.repository.ParameterDetailRepository;
import com.mpg.itcane.flow.repository.ParameterRepository;
import com.mpg.itcane.flow.util.JSONUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class FlowContinueTest {

	@Autowired
	 ParameterRepository parameterRepository;

	 @Autowired
	 ParameterDetailRepository parameterDetailRepository;
	 
	 @Autowired
	 FlowConfigRepository flowConfigRepository;
	 
	 @Autowired
	 FlowConfigDetailRepository flowConfigDetailRepository;
	 
	 @Autowired
	 FlowTaskRepository flowTaskRepository;

	 @Autowired
	 private WebApplicationContext wac;
	 private MockMvc mockMvc;
	 
	 @Before
	 public void setUp(){
	        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	 }

	 @After
	 public void tearDown() {
	 }
	 
	 private String simpleCreateFlow() throws Exception{
		 /* Request Setting */
	    	String systemCode = "ITCANE";
	    	String flowCode   = "F001";
	    	Map<String,Object> requestJsonBodyMap = new HashMap();
	    	
	    	List<Map<String,Object>> testDetailLs = new ArrayList();
	    	Map<String,Object> task1 = new HashMap();
	    	task1.put("sequence", 1);
	    	task1.put("usernameApprover", "sakunrat_r");
	    	task1.put("roleFlow", "VRF");
	    	task1.put("userId", "1234567");
	    	task1.put("email", "sakunrat_r@gmail.com");
	    	testDetailLs.add(task1);

	    	Map<String,Object> task2 = new HashMap();
	    	task2.put("sequence", 2);
	    	task2.put("usernameApprover", "suriya_e");
	    	task2.put("roleFlow", "APR");
	    	task2.put("userId", "7654321");
	    	task2.put("email", "suriya_e@gmail.com");
	    	testDetailLs.add(task2);
	    	

	    	requestJsonBodyMap.put("description", "test flow");
	    	requestJsonBodyMap.put("creater", "nipitphol_e");
	    	requestJsonBodyMap.put("createrEmail", "nipitphol_e@gmail.com");
	    	requestJsonBodyMap.put("requester", "nakarin_e");
	    	requestJsonBodyMap.put("requesterEmail", "nakarin_e@gmail.com");
	    	requestJsonBodyMap.put("details", testDetailLs);
	    	String requestJsonBody = JSONUtil.getJSONSerializer().include("details").serialize(requestJsonBodyMap) ;
	    	
	       
	    	/* Response Setting */
	    	/* Set Task */
	    	List<Map<String,Object>> responseJsonDataDetailLs = new ArrayList();
	    	Map<String,Object> responseJsonDataDetailMap1 = new HashMap();
	    	responseJsonDataDetailMap1.put("sequence", 1);
	    	responseJsonDataDetailMap1.put("approverUserName", "sakunrat_r");
	    	responseJsonDataDetailMap1.put("actionStateCode", "VRF");
	    	responseJsonDataDetailMap1.put("actionStateName", "Verify");
	    	responseJsonDataDetailMap1.put("taskStatus", "WAIT");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap1);
	    	
	    	
	    	Map<String,Object> responseJsonDataDetailMap2 = new HashMap();
	    	responseJsonDataDetailMap2.put("sequence", 2);
	    	responseJsonDataDetailMap2.put("approverUserName", "suriya_e");
	    	responseJsonDataDetailMap2.put("actionStateCode", "APR");
	    	responseJsonDataDetailMap2.put("actionStateName", "Approve");
	    	responseJsonDataDetailMap2.put("taskStatus", "NOT_ARRIVE");
	    	responseJsonDataDetailLs.add(responseJsonDataDetailMap2);
	    	
	    	/* Set Process */
	    	Map<String,Object> responseJsonDataHeaderMap = new HashMap();
	    	responseJsonDataHeaderMap.put("flowCode", "F001");
	    	responseJsonDataHeaderMap.put("description", "test flow");
	    	responseJsonDataHeaderMap.put("processStatus", "CREATED");
	    	responseJsonDataHeaderMap.put("requester", "nakarin_e");
	    	responseJsonDataHeaderMap.put("tasks", responseJsonDataDetailLs);
	    	
	    	/* Set Email */
	    	Map<String,Object> responseJsonDataEmailMap = new HashMap();
	    	responseJsonDataEmailMap.put("providerCode", "ITCANE1");
	    	responseJsonDataEmailMap.put("actionStateName", "Verify");
	    	responseJsonDataEmailMap.put("actionStateCode", "VRF");
	    	responseJsonDataEmailMap.put("approverName", "sakunrat_r");
	    	responseJsonDataEmailMap.put("approverUserId", "1234567");
	    	responseJsonDataEmailMap.put("taskStatus", "WAIT");
	    	responseJsonDataEmailMap.put("templateCode", "ITCANE1_CREATED");	
	    	responseJsonDataEmailMap.put("from", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("fromAddress", "itcane@gmail.com");
	    	responseJsonDataEmailMap.put("addressTo", "sakunrat_r@gmail.com");
	    	responseJsonDataEmailMap.put("ccAddress", "nakarin_e@gmail.com");
         
	    	/* Set Main Structure */
	    	Map<String,Object> responseJsonDataMap = new HashMap();
	    	responseJsonDataMap.put("processData", responseJsonDataHeaderMap);
	    	responseJsonDataMap.put("emailData", responseJsonDataEmailMap);
	    	
	    	Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("data", responseJsonDataMap);
	    	String responseJsonBody = JSONUtil.getJSONSerializer().include("data").include("data.processData.tasks").serialize(responseJsonBodyMap) ;
	        
	    	return mockMvc.perform(post("/createProcess/"+systemCode+"/"+flowCode)
	        		.contentType(MediaType.APPLICATION_JSON)
	        		.content(requestJsonBody)
	        		)
	                .andExpect(status().isCreated())
	                .andExpect(content().json(responseJsonBody))
	                .andReturn().getResponse().getContentAsString()
	                ;
	 }
	 
	 private String simpleApproveFlowComplete() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"message\":\"Approve successful.\","
			        		+"\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"CREATED\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"APPROVE\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"WAIT\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		));
		 
		 
		 /* Get Current Task of Process */
		 jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 String urlPath = "/sendActionToFlow/"+systemCode+ApplicationConstant.PATH_DELIMITER+processId+ApplicationConstant.PATH_DELIMITER+taskId+ApplicationConstant.PATH_DELIMITER;
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+ApplicationConstant.PATH_DELIMITER+processId+ApplicationConstant.PATH_DELIMITER+taskId+ApplicationConstant.PATH_DELIMITER+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "suriya_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"message\":\"Approve successful.\","
			        		+"\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"COMPLETE\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"APPROVE\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"APPROVE\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 return urlPath;
	 }
	 
	 private String simpleApproveFlowFirstStep() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"message\":\"Approve successful.\","
			        		+"\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"CREATED\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"APPROVE\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"WAIT\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		));
		 
		 
		 /* Get Current Task of Process */
		 jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 return "/sendActionToFlow/"+systemCode+ApplicationConstant.PATH_DELIMITER+processId+ApplicationConstant.PATH_DELIMITER+taskId+ApplicationConstant.PATH_DELIMITER;
	}
	 
	 @Test
	 public void approveFlowComplete() throws Exception  {
		 simpleApproveFlowComplete();
	 }
	 
	 @Test
	 public void approveFlowCompleteApprove() throws Exception  {
		 String urlPath = simpleApproveFlowComplete();
		 mockMvc.perform(get(urlPath+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "suriya_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been completed and cannot be approved.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
	 }
	 
	 @Test
	 public void approveFlowCompleteCancel() throws Exception  {
		 String urlPath = simpleApproveFlowComplete();
		 mockMvc.perform(get(urlPath+ApplicationConstant.USER_EVENT_TYPE_CANCEL)
					.param("username", "nakarin_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been completed and cannot be canceled.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
	 }
	 
	 @Test
	 public void approveFlowCompleteReject() throws Exception  {
		 String urlPath = simpleApproveFlowComplete();
		 mockMvc.perform(get(urlPath+ApplicationConstant.USER_EVENT_TYPE_REJECT)
					.param("username", "suriya_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been completed and cannot be rejected.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
	 }
	 
	 @Test
	 public void approveFlowCompleteTerminate() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"message\":\"Approve successful.\","
			        		+"\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"CREATED\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"APPROVE\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"WAIT\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		));
		 
		 
		 /* Get Current Task of Process */
		 jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "suriya_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"message\":\"Approve successful.\","
			        		+"\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"COMPLETE\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"APPROVE\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"APPROVE\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 mockMvc.perform(get("/terminateProcess/"+systemCode+"/"+processId))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been completed and cannot be terminated.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 
	 }

	 @Test
	 public void approveFlow1StepCancel() throws Exception  {
		 String urlPath  = simpleApproveFlowFirstStep();
		 mockMvc.perform(get(urlPath+ApplicationConstant.USER_EVENT_TYPE_CANCEL)
					.param("username", "nakarin_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Cancel successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"CANCEL\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"APPROVE\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"WAIT\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
	 }

	 @Test
	 public void approveFlow1StepReject() throws Exception  {
		 String urlPath  = simpleApproveFlowFirstStep();
		 mockMvc.perform(get(urlPath+ApplicationConstant.USER_EVENT_TYPE_REJECT)
					.param("username", "suriya_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Reject successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"REJECT\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"APPROVE\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"REJECT\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
	 }
	 

	 @Test
	 public void approveFlow1StepTerminate() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"message\":\"Approve successful.\","
			        		+"\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"CREATED\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"APPROVE\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"WAIT\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		));
		 
		 
		 /* Get Current Task of Process */
		 mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 
		 mockMvc.perform(get("/terminateProcess/"+systemCode+"/"+processId))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Terminate successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"TERMINATE\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"APPROVE\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"WAIT\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
	 }
	 
	 @Test
	 public void cancelFlowApprove() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_CANCEL)
					.param("username", "nakarin_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Cancel successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"CANCEL\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"WAIT\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been canceled and cannot be approved.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
	 }
	 
	 @Test
	 public void cancelFlowCancel() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_CANCEL)
					.param("username", "nakarin_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Cancel successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"CANCEL\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"WAIT\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_CANCEL)
					.param("username", "nakarin_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been canceled and cannot be canceled.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
	 }
	 

	 @Test
	 public void cancelFlowReject() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_CANCEL)
					.param("username", "nakarin_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Cancel successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"CANCEL\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"WAIT\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_REJECT)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been canceled and cannot be rejected.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
	 }
	 
	 @Test
	 public void cancelFlowTerminate() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_CANCEL)
					.param("username", "nakarin_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Cancel successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"CANCEL\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"WAIT\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 mockMvc.perform(get("/terminateProcess/"+systemCode+"/"+processId))
	        .andExpect(status().isOk())
	        .andExpect(content().json(
	        		"{"
	        		+ "\"success\":false,"
	        		+ "\"message\":\"The process has been canceled and cannot be terminated.\""
	        		+ "}"
	        		))
	        .andReturn().getResponse().getContentAsString();

	 }

	 @Test
	 public void rejectFlowApprove() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_REJECT)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Reject successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"REJECT\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"REJECT\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been rejected and cannot be approved.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
	 }
	 
	 @Test
	 public void rejectFlowCancel() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_REJECT)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Reject successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"REJECT\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"REJECT\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_CANCEL)
					.param("username", "nakarin_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been rejected and cannot be canceled.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
	 }
	 

	 @Test
	 public void rejectFlowReject() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_REJECT)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Reject successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"REJECT\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"REJECT\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_REJECT)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been rejected and cannot be rejected.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
	 }
	 
	 @Test
	 public void rejectFlowTerminate() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_REJECT)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":true,"
			        		+ "\"message\":\"Reject successful.\","
			        		+ "\"data\":{"
			        					+"\"processData\":{"
			        										+"\"processStatus\":\"REJECT\","
			        										+"\"tasks\":["
			        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"REJECT\"},"
			        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
			        										+ "]"
			        									+ "}"
			        				+ "}"
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 mockMvc.perform(get("/terminateProcess/"+systemCode+"/"+processId))
	        .andExpect(status().isOk())
	        .andExpect(content().json(
	        		"{"
	        		+ "\"success\":false,"
	        		+ "\"message\":\"The process has been rejected and cannot be terminated.\""
	        		+ "}"
	        		))
	        .andReturn().getResponse().getContentAsString();

	 }
	 
	 @Test
	 public void terminateFlowApprove() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/terminateProcess/"+systemCode+"/"+processId))
	        .andExpect(status().isOk())
	        .andExpect(content().json(
	        		"{"
	        		+ "\"success\":true,"
	        		+ "\"message\":\"Terminate successful.\","
	        		+ "\"data\":{"
	        					+"\"processData\":{"
	        										+"\"processStatus\":\"TERMINATE\","
	        										+"\"tasks\":["
	        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"WAIT\"},"
	        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
	        										+ "]"
	        									+ "}"
	        				+ "}"
	        		+ "}"
	        		))
	        .andReturn().getResponse().getContentAsString();
		 
		 
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_APPROVE)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been terminated and cannot be approved.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 

	 }


	 @Test
	 public void terminateFlowCancel() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/terminateProcess/"+systemCode+"/"+processId))
	        .andExpect(status().isOk())
	        .andExpect(content().json(
	        		"{"
	        		+ "\"success\":true,"
	        		+ "\"message\":\"Terminate successful.\","
	        		+ "\"data\":{"
	        					+"\"processData\":{"
	        										+"\"processStatus\":\"TERMINATE\","
	        										+"\"tasks\":["
	        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"WAIT\"},"
	        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
	        										+ "]"
	        									+ "}"
	        				+ "}"
	        		+ "}"
	        		))
	        .andReturn().getResponse().getContentAsString();
		 
		 
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_CANCEL)
					.param("username", "nakarin_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been terminated and cannot be canceled.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 

	 }
	 
	 @Test
	 public void terminateFlowReject() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 Integer taskId = JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id").intValue();
		 
		 mockMvc.perform(get("/terminateProcess/"+systemCode+"/"+processId))
	        .andExpect(status().isOk())
	        .andExpect(content().json(
	        		"{"
	        		+ "\"success\":true,"
	        		+ "\"message\":\"Terminate successful.\","
	        		+ "\"data\":{"
	        					+"\"processData\":{"
	        										+"\"processStatus\":\"TERMINATE\","
	        										+"\"tasks\":["
	        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"WAIT\"},"
	        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
	        										+ "]"
	        									+ "}"
	        				+ "}"
	        		+ "}"
	        		))
	        .andReturn().getResponse().getContentAsString();
		 
		 
		 
		 mockMvc.perform(get("/sendActionToFlow/"+systemCode+"/"+processId+"/"+taskId+"/"+ApplicationConstant.USER_EVENT_TYPE_REJECT)
					.param("username", "sakunrat_r"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(
			        		"{"
			        		+ "\"success\":false,"
			        		+ "\"message\":\"The process has been terminated and cannot be rejected.\""
			        		+ "}"
			        		))
			        .andReturn().getResponse().getContentAsString();
		 
		 

	 }
	 
	 @Test
	 public void terminateFlowTerminate() throws Exception  {
		 String systemCode = "ITCANE";
		 String jsonCreateFlow  = simpleCreateFlow();
		 Integer processId = JSONUtil.fromJsonPath(jsonCreateFlow, Double.class, "data.processData.id").intValue();
		 
		 /* Get Current Task of Process */
		 String jsonCurrentTaskReturn = mockMvc.perform(get("/getCurrentTask/"+processId)) .andExpect(status().isOk()) .andReturn().getResponse().getContentAsString();
		 JSONUtil.fromJsonPath(jsonCurrentTaskReturn, Double.class, "data.id");
		 
		 mockMvc.perform(get("/terminateProcess/"+systemCode+"/"+processId))
	        .andExpect(status().isOk())
	        .andExpect(content().json(
	        		"{"
	        		+ "\"success\":true,"
	        		+ "\"message\":\"Terminate successful.\","
	        		+ "\"data\":{"
	        					+"\"processData\":{"
	        										+"\"processStatus\":\"TERMINATE\","
	        										+"\"tasks\":["
	        											+ "{\"actionStateCode\":\"VRF\",\"approverUserName\":\"sakunrat_r\",\"taskStatus\":\"WAIT\"},"
	        											+ "{\"actionStateCode\":\"APR\",\"approverUserName\":\"suriya_e\",\"taskStatus\":\"NOT_ARRIVE\"}"
	        										+ "]"
	        									+ "}"
	        				+ "}"
	        		+ "}"
	        		));
		 
		 mockMvc.perform(get("/terminateProcess/"+systemCode+"/"+processId))
	        .andExpect(status().isOk())
	        .andExpect(content().json(
	        		"{"
	        		+ "\"success\":false,"
	        		+ "\"message\":\"The process has been terminated and cannot be terminated.\""
	        		+ "}"
	        		));
		 

	 }
	 
	 @Test
	 public void findEndProcessByCreater() throws Exception  {
	    	Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Has 1 process of nipitphol_e");
	    	String responseJsonBody = JSONUtil.getJSONSerializer().serialize(responseJsonBodyMap) ;
	       
	    	
	    	simpleApproveFlowComplete();
			mockMvc.perform(get("/findEndProcessByCreater/"+"nipitphol_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 

	 
	 @Test
	 public void findEndProcessAndTaskDetailByCreater() throws Exception  {
	    	Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Has 1 process of nipitphol_e");
	    	String responseJsonBody = JSONUtil.getJSONSerializer().serialize(responseJsonBodyMap) ;
	       
	    	
	    	simpleApproveFlowComplete();
			mockMvc.perform(get("/findEndProcessAndTaskDetailByCreater/"+"nipitphol_e"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void findEndProcessByCreaterAndFlowCode() throws Exception  {
	    	Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Has 1 process of nipitphol_e");
	    	String responseJsonBody = JSONUtil.getJSONSerializer().serialize(responseJsonBodyMap) ;
	       
	    	
	    	simpleApproveFlowComplete();
			mockMvc.perform(get("/findEndProcessByCreaterAndFlowCode/"+"nipitphol_e"+ApplicationConstant.PATH_DELIMITER+"F001"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }
	 
	 @Test
	 public void findEndProcessAndTaskDetailByCreaterAndFlowCode() throws Exception  {
	    	Map<String,Object> responseJsonBodyMap = new HashMap();
	    	responseJsonBodyMap.put("success", true);
	    	responseJsonBodyMap.put("message", "Has 1 process of nipitphol_e");
	    	String responseJsonBody = JSONUtil.getJSONSerializer().serialize(responseJsonBodyMap) ;
	       
	    	
	    	simpleApproveFlowComplete();
			mockMvc.perform(get("/findEndProcessAndTaskDetailByCreaterAndFlowCode/"+"nipitphol_e"+ApplicationConstant.PATH_DELIMITER+"F001"))
			        .andExpect(status().isOk())
			        .andExpect(content().json(responseJsonBody))
			        .andReturn().getResponse().getContentAsString();
	  }

}
